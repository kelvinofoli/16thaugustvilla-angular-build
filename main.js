(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\C833240.STANBICGHAN\Projects\16th-august-85-villa\Frontend\src\main.ts */"zUnb");


/***/ }),

/***/ "6Y1P":
/*!************************************************!*\
  !*** ./src/app/property/property.component.ts ***!
  \************************************************/
/*! exports provided: PropertyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropertyComponent", function() { return PropertyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../footer/footer.component */ "fp1T");





function PropertyComponent_div_58_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.prop.BAs);
} }
class PropertyComponent {
    constructor(route) {
        this.route = route;
        this.propertyData = [
            { name: 'Property name', price: 'Price', description: 'description ', rooms: '4 BD', BAs: 'not', images: 1 },
            { name: 'East Legon Hills', price: '$140,000', description: 'Status: Newly Built ', rooms: '4 BD', BAs: 'not', images: 6 },
            { name: 'Appiadu (landmark KNUST)', price: 'GHc 700,000',
                description: `1. Bedrooms--4${'\n'}2. Washrooms--5${'\n'}3. Kitchen--YES${'\n'}4. Purpose--Residential${'\n'}5. Location--Appiadu (land mark KNUST)`, rooms: '4 BD', BAs: '5 BA', images: 6 },
            { name: 'Ahinsen (Landmark Kumasi High SHS/KNUST)', price: 'GHc 900,000', description: `1. Bedrooms--4${'\n'}2. Washrooms--4.5${'\n'}3. Kitchen--YES${'\n'}4. Purpose--Residential${'\n'}5. Budget--900k GHc (Payment plan maybe accepted depending on the aggreement of payments)${'\n'}6. Ahinsen (Land mark Kumasi High SHS/KNUST)`, rooms: '4 BD', BAs: '4.5 BA', images: 6 },
            { name: 'Old Ashongman-Accra, 15 minutes drive from Legon Botanical Gardens.', price: 'GHc 420,000', description: `3 Bedrooms Detached House For Sale,${'\n'}Location: Old Ashongman-Accra, 15 minutes drive from Legon Botanical Gardens.`, rooms: '3 BD', BAs: 'not', images: 6 },
            { name: 'Oyarifa', price: '$250,000', description: `4 Bedrooms furnished house for sale at Oyarifa`, rooms: '4 BD', BAs: 'not', images: 6 },
            { name: 'At East legon- School Junction (with 1 Boys quaters)', price: '$250,000', description: `Executive 5 Bedeoom${'\n'}House with  1 Boys quaters and a swimming pool for sale  At East legon- School Junction${'\n'}#Price $ 250,000${'\n'}Slightly Negotiable${'\n'}#Status  Newly Built`, rooms: '5 BD', BAs: 'not', images: 6 },
            { name: '4 Bedroom House for sale at Roman Ridge ', price: '$450,000', description: `All bedrooms ensuite${'\n'}*Air-conditioned rooms${'\n'}*Wardrobes available${'\n'}*Polytank provided${'\n'}*Large and spacious compound${'\n'}*Beautiful Landscaping with Green grass and a Garden.${'\n'}Close to Roadside for easy access.In a very secure area.`, rooms: '4 BD', BAs: 'not', images: 6 },
            { name: 'Luxurious 3 Bedroom House For Sale At East Legon Hills', price: '$110,000', description: `Luxurious 3 Bedroom House For Sale At East Legon Hills${'\n'}located In A Gated Community ${'\n'}just A Min Drive Away From The Main Road.${'\n'}- House Is Fully Walled And Gated With Electric Fencing ${'\n'}Paved Compound With Lawn Area.${'\n'}- Ample Parking Space ${'\n'},Air Conditioners In All Rooms${'\n'}Burglarproofed Sliding Windows${'\n'}Very Large Room Sizes${'\n'}Ultramodern Lighting Systems${'\n'}- Inbuilt Wall Wardrobes In All Rooms${'\n'}- All Rooms Well Tiled${'\n'}- Spacious Kitchens With Fitted Sinks, Cabinets And Storeroom `, rooms: '3 BD', BAs: 'not', images: 6 },
            { name: '5 Bedroom House for sale at Roman Ridge ', price: '$110,000', description: `*All bedrooms ensuite${'\n'}*Air-conditioned rooms${'\n'}*Wardrobes available${'\n'}*Polytank provided${'\n'}*Large and spacious compound${'\n'}*Beautiful Landscaping with Green grass and a Garden.${'\n'}Close to Roadside for easy access.${'\n'}In a very secure area.`, rooms: '5 BD', BAs: 'not', images: 6 },
            { name: '4 bedrooms town house letting in East Legon, Accra.', price: 'Contact for Price...', description: `An exclusive community of 4 Bedroom Townhouses in East Legon’s most primed neighbourhood, just off Mensah Wood Street, behind Galaxy International School.${'\n'}The development offers 24 hour security, back-up utilities and swimming pool. Each house provides 4 double bedrooms and one single room, plus stylish open-plan living spaces and private terrace garden.`, rooms: '4 BD', BAs: 'not', images: 6 },
            { name: '80% Completed 12 Apartments with 3 bedrooms each', price: '$750,000', description: `80% Completed 12 Apartments with 3 bedrooms each, with large compound For Sale at Kwadaso Hilltop Estate Kumasi Ghana.`, rooms: '4 BD x 12', BAs: 'not', images: 6 },
        ];
    }
    ngOnInit() {
        this.route.paramMap.subscribe((params) => {
            this.id = params.get('id');
            // this.id = '0'
        });
        this.images = [1, 2, 3, 4, 5, 6].map((n) => `../../assets/images/apartments/${this.id}/${n}.jpg`);
        this.prop = this.propertyData[this.id];
    }
}
PropertyComponent.ɵfac = function PropertyComponent_Factory(t) { return new (t || PropertyComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"])); };
PropertyComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PropertyComponent, selectors: [["app-property"]], decls: 60, vars: 5, consts: [["id", "home"], [1, "animate"], [1, "container"], [1, "logo", "left"], ["routerLink", "/"], ["src", "../../assets/images/logo_.png", 1, "logo"], [1, "menu-button", "hide", "right", "pointer"], [1, "menu", "left"], [1, "page-menu", "left"], [1, "registration", "flex-center"], [1, "join-us"], [1, "pointer", "animate"], ["href", "#contact"], [1, "getting-started"], ["routerLink", "/", 1, "main-btn", "pointer", "text-center", "animate"], [1, "property"], [1, "row"], [1, "col-12"], [1, "col-12", "col-lg-6"], ["id", "carouselExampleIndicators", "data-ride", "carousel", 1, "carousel", "slide"], [1, "carousel-indicators"], [1, "carousel-inner"], [1, "carousel-item", "active"], ["src", "../../assets/images/apartments/temp.jpg", "alt", "...", 1, "d-block", "w-100"], [1, "arrows"], ["onclick", "$(this).closest('.carousel').carousel('next');", "role", "button", "data-slide", "prev", 1, "carousel-control-prev"], ["aria-hidden", "true", 1, "carousel-control-prev-icon"], [1, "sr-only"], ["onclick", "$(this).closest('.carousel').carousel('next');", "role", "button", "data-slide", "next", 1, "carousel-control-next"], ["aria-hidden", "true", 1, "carousel-control-next-icon"], [1, "details", "col-12", "col-lg-6"], [1, "price"], [1, "bedrooms", "right", "flex-center"], ["src", "../../assets/images/bed.svg", "alt", "", 1, "left"], [1, "left"], ["class", "bathrooms right flex-center", 4, "ngIf"], [1, "bathrooms", "right", "flex-center"], ["src", "../../assets/images/shower.svg", "alt", "", 1, "left"]], template: function PropertyComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "ol", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "img", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "span", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "span", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Previous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "span", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "p", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, PropertyComponent_div_58_Template, 4, 1, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "app-footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.prop.name);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.prop.price);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.prop.description, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.prop.rooms);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.prop.BAs !== "not");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]], styles: ["*[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0;\r\n}\r\nhtml[_ngcontent-%COMP%] {\r\n  box-sizing: border-box;\r\n}\r\n*[_ngcontent-%COMP%], *[_ngcontent-%COMP%]:before, *[_ngcontent-%COMP%]:after {\r\n  box-sizing: inherit;\r\n}\r\na[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  position: relative;\r\n  color: #1F373D;\r\n}\r\nbody[_ngcontent-%COMP%] {\r\n  -webkit-font-smoothing: antialiased;\r\n  -moz-osx-font-smoothing: grayscale;\r\n  text-rendering: optimizeLegibility;\r\n  text-shadow: rgba(0, 0, 0, .01) 0 0 1px;\r\n}\r\na[_ngcontent-%COMP%]:focus, a[_ngcontent-%COMP%]:hover {\r\n  text-decoration: none;\r\n  color: #ffd000;\r\n}\r\nimg[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n  height: auto;\r\n}\r\n[_ngcontent-%COMP%]::selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n[_ngcontent-%COMP%]::-moz-selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n[_ngcontent-%COMP%]::-webkit-selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n[_ngcontent-%COMP%]:active, [_ngcontent-%COMP%]:focus {\r\n  outline: none !important;\r\n}\r\n.text-left[_ngcontent-%COMP%] {\r\n  text-align: left !important;\r\n}\r\n.text-right[_ngcontent-%COMP%] {\r\n  text-align: right !important;\r\n}\r\n.text-center[_ngcontent-%COMP%] {\r\n  text-align: center !important;\r\n}\r\n.left[_ngcontent-%COMP%] {\r\n  float: left;\r\n}\r\n.right[_ngcontent-%COMP%] {\r\n  float: right;\r\n}\r\n.center[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n}\r\n.flex-center[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n.uppercase[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n}\r\n.white-text[_ngcontent-%COMP%] {\r\n  color: #FFF;\r\n}\r\n.light-bg[_ngcontent-%COMP%] {\r\n  background-color: #fff;\r\n}\r\n.yellow-bg[_ngcontent-%COMP%] {\r\n  background-color: #ffd000;\r\n}\r\n.pointer[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n}\r\n.hide[_ngcontent-%COMP%] {\r\n  display: none;\r\n}\r\n.show[_ngcontent-%COMP%] {\r\n  display: block;\r\n}\r\n.slide[_ngcontent-%COMP%] {\r\n  left: 0;\r\n  transition: all 0.4s ease-in-out;\r\n}\r\n\r\nbody[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n  font-family: 'Montserrat', sans-serif;\r\n  font-size: 13pt;\r\n  font-weight: 400;\r\n  overflow-x: hidden;\r\n  transition: opacity 1s;\r\n}\r\nh1[_ngcontent-%COMP%], .h1[_ngcontent-%COMP%] {\r\n  font-size: 58px;\r\n  font-weight: 700;\r\n}\r\nh2[_ngcontent-%COMP%], .h2[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n}\r\nh3[_ngcontent-%COMP%], .h3[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n}\r\nh4[_ngcontent-%COMP%], .h4[_ngcontent-%COMP%] {\r\n  font-size: 25px;\r\n  font-weight: 700;\r\n}\r\nh5[_ngcontent-%COMP%], .h5[_ngcontent-%COMP%] {\r\n  font-size: 14px;\r\n  font-weight: 400;\r\n  color: #919EB1;\r\n}\r\nh6[_ngcontent-%COMP%], .h6[_ngcontent-%COMP%] {\r\n  font-size: 14px;\r\n}\r\np[_ngcontent-%COMP%] {\r\n  font-size: 16px;\r\n  line-height: 1.8;\r\n  margin-bottom: 15px;\r\n  letter-spacing: 0.025em;\r\n}\r\nli[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  font-weight: 400;\r\n  letter-spacing: 0.025em;\r\n  display: inline;\r\n}\r\n.animate[_ngcontent-%COMP%] {\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n.lux-shadow[_ngcontent-%COMP%] {\r\n  box-shadow: 0px 10px 15px 0px rgba(29, 31, 36, 0.1);\r\n}\r\n\r\nheader[_ngcontent-%COMP%] {\r\n  padding-bottom: 25px;\r\n}\r\nheader[_ngcontent-%COMP%]   nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 95px;\r\n  position: fixed;\r\n  z-index: 9999;\r\n  background-color: #fff;\r\n}\r\n.small-nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 70px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n.logo[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-start;\r\n  font-size: 18pt;\r\n  font-weight: 700;\r\n  width: 120px;\r\n  \r\n}\r\n.menu[_ngcontent-%COMP%] {\r\n  width: 80%;\r\n  height: 95px;\r\n}\r\n.page-menu[_ngcontent-%COMP%] {\r\n  width: 70%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n.registration[_ngcontent-%COMP%] {\r\n  width: 30%;\r\n  height: 95px;\r\n}\r\n.join-us[_ngcontent-%COMP%] {\r\n  width: 40%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n  padding-right: 20px;\r\n}\r\n.getting-started[_ngcontent-%COMP%] {\r\n  width: 60%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n}\r\n.main-btn[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  background-color: #ffd000;\r\n  padding: 10px 10px;\r\n  margin: 0px 5px;\r\n  border-radius: 5px;\r\n  font-size: 13px;\r\n  border: 1px solid #ffd000;\r\n  transition: 0.4s all;\r\n}\r\n.main-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: transparent;\r\n  color: #000000;\r\n  border: 1px solid #ffd000;\r\n}\r\n.main-btn[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n}\r\n.main-btn[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #fff;\r\n}\r\n.page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n  padding: 8px;\r\n}\r\n.active[_ngcontent-%COMP%] {\r\n  color: #ffd000;\r\n}\r\n\r\n.hero[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 700px;\r\n  position: relative;\r\n}\r\n.title[_ngcontent-%COMP%] {\r\n  width: 60%;\r\n  height: 30vh;\r\n  position: absolute;\r\n  z-index: 999;\r\n  top: 23vh;\r\n}\r\n.hero-image[_ngcontent-%COMP%] {\r\n  width: 88%;\r\n  height: 630px;\r\n  float: right;\r\n\r\n  background-image: url('hero.png');\r\n  background-size: cover;\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  position: relative;\r\n  right: -60px;\r\n}\r\n.hero-image-info[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  right: 0;\r\n  bottom: 0;\r\n}\r\n.hero-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  font-size: 18px;\r\n  padding: 10px 20px;\r\n  margin-top: 15px;\r\n}\r\n.info[_ngcontent-%COMP%] {\r\n  margin: 5px 35px;\r\n}\r\n.info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: #fff\r\n}\r\n.search[_ngcontent-%COMP%] {\r\n  margin-top: 40px;\r\n  width: 500px;\r\n  position: absolute;\r\n}\r\n.search[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n  width: 75%;\r\n  background: url('search.svg') no-repeat 8px 8px;\r\n  background-position: left;\r\n  border: 0px;\r\n  background-color: #fff;\r\n  padding: 25px 40px;\r\n  font-size: 15px;\r\n  border-top-left-radius: 5px;\r\n  border-bottom-left-radius: 5px;\r\n  background-position: left 15px top 29px;\r\n}\r\n.search-btn[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  border: 0px;\r\n  background-color: #ffd000;\r\n  color: #fff;\r\n  padding: 25px 10px;\r\n  font-size: 15px;\r\n  font-weight: 700;\r\n  border-top-right-radius: 5px;\r\n  border-bottom-right-radius: 5px;\r\n}\r\n.search-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: #ffd000;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n[_ngcontent-%COMP%]::placeholder {\r\n  color: #919EB1;\r\n}\r\n.hero-wrapper[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  width: 100%;\r\n  height: 725px;\r\n  padding-top: 95px;\r\n}\r\n.slide-down[_ngcontent-%COMP%] {\r\n  width: 20px;\r\n  height: 65px;\r\n  position: absolute;\r\n  bottom: 0;\r\n\r\n}\r\n.slide-down[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  transform: rotate(-90deg);\r\n  font-size: 12px;\r\n  white-space: nowrap;\r\n  color: #919EB1;\r\n}\r\n\r\n.property[_ngcontent-%COMP%] {\r\n    background-color: #F6F5F4;\r\n    padding: 60px 0px 70px 0px;\r\n}\r\n\r\nfooter[_ngcontent-%COMP%] {\r\n    background-color: #333333;\r\n    width: 100%;\r\n    color: #fff;\r\n    padding: 65px 0px;\r\n  }\r\n.footer-top[_ngcontent-%COMP%] {\r\n    border-bottom: 1px solid #919EB1;\r\n    padding-bottom: 45px;\r\n  }\r\n.Newsletter[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n    height: 70px;\r\n    padding: 0px 15px;\r\n    border: 0px;\r\n    margin-top: 26px;\r\n  }\r\n.newsletter-btn[_ngcontent-%COMP%] {\r\n    background-color: #fbfbfb;\r\n    width: 20%;\r\n    height: 70px;\r\n    border: 0px;\r\n    background-image: url('arrow-right.svg');\r\n    background-repeat: no-repeat;\r\n    background-position: center;\r\n    margin-top: 26px;\r\n  }\r\n.newsletter-btn[_ngcontent-%COMP%]:hover {\r\n    background-color: #ffd000;\r\n    transition: all 0.3s ease-in-out;\r\n  }\r\n.footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n    margin-bottom: 60px;\r\n  }\r\n.footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 13px;\r\n    font-weight: 400;\r\n  }\r\n.footer-bottom[_ngcontent-%COMP%] {\r\n    padding-top: 70px;\r\n  }\r\n.footer-bottom[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    color: white;\r\n    font-size: 15px;\r\n  }\r\n.footer-column[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%] {\r\n    padding-bottom: 35px;\r\n  }\r\n.footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    color: #fff;\r\n    display: flex;\r\n    font-size: 13px;\r\n    padding-bottom: 18px;\r\n  }\r\n\r\n\r\n@media only screen and (max-width: 992px) {\r\n    .menu[_ngcontent-%COMP%] {\r\n      background-color: #fff;\r\n      width: 100%;\r\n      height: 100vh;\r\n      position: absolute;\r\n      top: 70px;\r\n      right: 100%;\r\n      text-align: center;\r\n      z-index: 999;\r\n      transition: all 0.4s ease-in-out;\r\n    }\r\n  \r\n    .page-menu[_ngcontent-%COMP%] {\r\n      width: 100%;\r\n      height: 50vh;\r\n      text-align: center;\r\n      float: none;\r\n      display: flex;\r\n      flex-direction: column;\r\n    }\r\n  \r\n    .page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n      font-size: 15pt;\r\n      padding: 1.5vh;\r\n    }\r\n  \r\n    .registration[_ngcontent-%COMP%] {\r\n      float: none;\r\n      width: 100%;\r\n      height: 50vh;\r\n      display: flex;\r\n      flex-direction: column;\r\n      justify-content: flex-start;\r\n      text-align: center;\r\n      padding-bottom: 5vh;\r\n    }\r\n  \r\n    .join-us[_ngcontent-%COMP%] {\r\n      width: 100%;\r\n      text-align: center;\r\n      height: 0px;\r\n      margin-top: 30px;\r\n    }\r\n  \r\n    .join-us[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n      width: 100%;\r\n      text-align: center;\r\n    }\r\n  \r\n    .menu-button[_ngcontent-%COMP%] {\r\n      width: 40px;\r\n      height: 40px;\r\n      display: block;\r\n      margin-top: 26px;\r\n    }\r\n  \r\n    .menu-button[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\r\n      width: 40px;\r\n      height: 5px;\r\n      background-color: #1F373D;\r\n      display: block;\r\n      margin: 6px 0px;\r\n    }\r\n  \r\n    .main-btn[_ngcontent-%COMP%] {\r\n      width: 100%;\r\n      background-color: #ffd000;\r\n      padding: 10px 10px;\r\n      margin: 0px;\r\n      border-radius: 5px;\r\n      font-size: 13px;\r\n      border: 1px solid #ffd000;\r\n      transition: 0.4s all;\r\n    }\r\n  \r\n    .statistic-box[_ngcontent-%COMP%]:nth-child(1) {\r\n      float: left;\r\n      margin-top: 50px;\r\n    }\r\n  \r\n    .statistic-box[_ngcontent-%COMP%]:nth-child(2) {\r\n      margin-top: 50px;\r\n    }\r\n  \r\n    .statistic-box[_ngcontent-%COMP%]:nth-child(3) {\r\n      float: left;\r\n    }\r\n  \r\n    .search-appartments[_ngcontent-%COMP%] {\r\n      width: 300px;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 30%;\r\n    }\r\n  \r\n    .how-it-works[_ngcontent-%COMP%] {\r\n      text-align: center;\r\n    }\r\n  }\r\n@media only screen and (max-width: 768px) {\r\n    .hero-image[_ngcontent-%COMP%] {\r\n      width: 100%;\r\n    }\r\n  \r\n    .info[_ngcontent-%COMP%] {\r\n      margin: 0px 10px;\r\n    }\r\n  \r\n    h1[_ngcontent-%COMP%] {\r\n      font-size: 45px;\r\n    }\r\n  \r\n    .search-appartments[_ngcontent-%COMP%] {\r\n      width: 270px;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 37%;\r\n      margin-left: 10px;\r\n    }\r\n  \r\n    .work-service-image[_ngcontent-%COMP%] {\r\n      max-height: 630px;\r\n      left: 0px;\r\n      margin-bottom: 50px;\r\n    }\r\n  \r\n    .footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n      float: left;\r\n    }\r\n  \r\n    .footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n      float: right;\r\n    }\r\n  }\r\n@media only screen and (max-width: 650px) {\r\n    .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n      font-size: 14px;\r\n    }\r\n  \r\n    .hero-image-info[_ngcontent-%COMP%] {\r\n      right: 40px;\r\n    }\r\n  \r\n    .title[_ngcontent-%COMP%] {\r\n      top: 17vh;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 40%;\r\n      margin-left: 5px;\r\n    }\r\n  }\r\n@media only screen and (max-width: 575px) {\r\n    .footer-bottom[_ngcontent-%COMP%] {\r\n      text-align: center;\r\n    }\r\n  \r\n    .footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n      display: list-item;\r\n      list-style: none;\r\n    }\r\n  }\r\n@media only screen and (max-width: 540px) {\r\n    .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n      font-size: 12px;\r\n    }\r\n  \r\n    .hero-image-info[_ngcontent-%COMP%] {\r\n      width: 90%;\r\n    }\r\n  \r\n    .search[_ngcontent-%COMP%] {\r\n      width: 400px;\r\n    }\r\n  \r\n    .search-appartments[_ngcontent-%COMP%] {\r\n      width: 80%;\r\n      margin: 0 auto;\r\n      margin-top: 30px;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 80%;\r\n      margin: 0px 10%;\r\n      margin-top: 15px;\r\n    }\r\n  }\r\n@media only screen and (max-width: 470px) {\r\n    .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n      font-size: 12px;\r\n    }\r\n  \r\n    .hero-image-info[_ngcontent-%COMP%] {\r\n      right: 30px;\r\n      width: 91%;\r\n    }\r\n  \r\n    .hero-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n      padding: 5px 5px;\r\n      font-size: 11px;\r\n    }\r\n  \r\n    .search[_ngcontent-%COMP%] {\r\n      width: 300px;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 80%;\r\n    }\r\n  }\r\n@media only screen and (max-width: 380px) {\r\n    .title[_ngcontent-%COMP%] {\r\n      top: 22vh;\r\n    }\r\n  \r\n    .search[_ngcontent-%COMP%] {\r\n      width: 250px;\r\n    }\r\n  \r\n    .search-btn[_ngcontent-%COMP%] {\r\n      width: 30%;\r\n    }\r\n  \r\n    .search-all-btn[_ngcontent-%COMP%] {\r\n      width: 80%;\r\n    }\r\n  \r\n    .search[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n      width: 70%;\r\n      font-size: 12px;\r\n      padding: 27px 40px;\r\n    }\r\n  \r\n    .info[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n      display: none;\r\n    }\r\n  }\r\n\r\n.details[_ngcontent-%COMP%]{\r\n      margin-left: 20px;\r\n      \r\n      max-width: 300px;\r\n      padding: 20px\r\n  }\r\n.details[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    white-space: pre-wrap;\r\n  }\r\n.flex-center[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n  }\r\n.price[_ngcontent-%COMP%] {\r\n    \r\n    \r\n  }\r\n.price[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 18px;\r\n    color: #ffd000;\r\n    font-weight: 700;\r\n  }\r\n.bathrooms[_ngcontent-%COMP%] {\r\n    width: 25%;\r\n    height: 100%;\r\n  }\r\n.bathrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    padding: 15px 0px 0px 10px;\r\n    font-size: 13px;\r\n    color: #919EB1;\r\n  }\r\n.bedrooms[_ngcontent-%COMP%] {\r\n    width: 50%;\r\n    height: 100%\r\n  }\r\n.bedrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    padding: 15px 0px 0px 10px;\r\n    font-size: 13px;\r\n    color: #919EB1;\r\n  }\r\n.arrows[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    \r\n    margin: auto;\r\n    position: absolute;\r\n    top: 0; left: 0; bottom: 0; right: 0;\r\n    padding: 5px;\r\n    align-items: center;\r\n    justify-content: space-between;\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3BlcnR5LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OzhEQUU4RDtBQUM5RDtFQUNFLG9CQUFvQjtFQUNwQixVQUFVO0FBQ1o7QUFFQTtFQUdFLHNCQUFzQjtBQUN4QjtBQUVBOzs7RUFLRSxtQkFBbUI7QUFDckI7QUFFQTtFQUNFLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsY0FBYztBQUNoQjtBQUVBO0VBQ0UsbUNBQW1DO0VBQ25DLGtDQUFrQztFQUNsQyxrQ0FBa0M7RUFDbEMsdUNBQXVDO0FBQ3pDO0FBRUE7O0VBRUUscUJBQXFCO0VBQ3JCLGNBQWM7QUFDaEI7QUFFQTtFQUNFLGVBQWU7RUFDZixZQUFZO0FBQ2Q7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsaUJBQWlCO0FBQ25CO0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7QUFFQTs7RUFFRSx3QkFBd0I7QUFDMUI7QUFFQTtFQUNFLDJCQUEyQjtBQUM3QjtBQUVBO0VBQ0UsNEJBQTRCO0FBQzlCO0FBRUE7RUFDRSw2QkFBNkI7QUFDL0I7QUFFQTtFQUNFLFdBQVc7QUFDYjtBQUVBO0VBQ0UsWUFBWTtBQUNkO0FBRUE7RUFDRSxjQUFjO0FBQ2hCO0FBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QjtBQUVBO0VBQ0UseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFFQTtFQUNFLHNCQUFzQjtBQUN4QjtBQUVBO0VBQ0UseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxlQUFlO0FBQ2pCO0FBRUE7RUFDRSxhQUFhO0FBQ2Y7QUFFQTtFQUNFLGNBQWM7QUFDaEI7QUFFQTtFQUNFLE9BQU87RUFHUCxnQ0FBZ0M7QUFDbEM7QUFFQTs7NkRBRTZEO0FBQzdEO0VBQ0UsY0FBYztFQUNkLHFDQUFxQztFQUNyQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixzQkFBc0I7QUFDeEI7QUFFQTs7RUFFRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBRUE7O0VBRUUsZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjtBQUVBOztFQUVFLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7QUFFQTs7RUFFRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBRUE7O0VBRUUsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCO0FBRUE7O0VBRUUsZUFBZTtBQUNqQjtBQUVBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCO0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixlQUFlO0FBQ2pCO0FBRUE7RUFHRSxnQ0FBZ0M7QUFDbEM7QUFFQTtFQUdFLG1EQUFtRDtBQUNyRDtBQUVBOzs4REFFOEQ7QUFDOUQ7RUFDRSxvQkFBb0I7QUFDdEI7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7QUFDekI7QUFFQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QjtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDtBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixtQkFBbUI7QUFDckI7QUFFQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix5QkFBeUI7QUFDM0I7QUFFQTtFQUNFLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixvQkFBb0I7QUFDdEI7QUFFQTtFQUNFLDZCQUE2QjtFQUM3QixjQUFjO0VBQ2QseUJBQXlCO0FBQzNCO0FBRUE7RUFDRSxjQUFjO0FBQ2hCO0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsY0FBYztBQUNoQjtBQUVBLGtCQUFrQjtBQUNsQjtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osU0FBUztBQUNYO0FBRUE7RUFDRSxVQUFVO0VBQ1YsYUFBYTtFQUNiLFlBQVk7O0VBRVosaUNBQXFEO0VBQ3JELHNCQUFzQjtFQUN0QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLGtCQUFrQjtFQUNsQixZQUFZO0FBQ2Q7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztBQUNYO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjtBQUVBO0VBQ0U7QUFDRjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLFVBQVU7RUFDViwrQ0FBbUU7RUFDbkUseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZiwyQkFBMkI7RUFDM0IsOEJBQThCO0VBQzlCLHVDQUF1QztBQUN6QztBQUVBO0VBQ0UsVUFBVTtFQUNWLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLDRCQUE0QjtFQUM1QiwrQkFBK0I7QUFDakM7QUFFQTtFQUNFLHlCQUF5QjtFQUd6QixnQ0FBZ0M7QUFDbEM7QUFFQTtFQUNFLGNBQWM7QUFDaEI7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsYUFBYTtFQUNiLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsU0FBUzs7QUFFWDtBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0FBYztBQUNoQjtBQUlBLGFBQWE7QUFDYjtJQUNJLHlCQUF5QjtJQUN6QiwwQkFBMEI7QUFDOUI7QUFHQTs7OERBRThEO0FBQzlEO0lBQ0kseUJBQXlCO0lBQ3pCLFdBQVc7SUFDWCxXQUFXO0lBQ1gsaUJBQWlCO0VBQ25CO0FBRUE7SUFDRSxnQ0FBZ0M7SUFDaEMsb0JBQW9CO0VBQ3RCO0FBRUE7SUFDRSxVQUFVO0lBQ1YsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixXQUFXO0lBQ1gsZ0JBQWdCO0VBQ2xCO0FBRUE7SUFDRSx5QkFBeUI7SUFDekIsVUFBVTtJQUNWLFlBQVk7SUFDWixXQUFXO0lBQ1gsd0NBQTREO0lBQzVELDRCQUE0QjtJQUM1QiwyQkFBMkI7SUFDM0IsZ0JBQWdCO0VBQ2xCO0FBRUE7SUFDRSx5QkFBeUI7SUFHekIsZ0NBQWdDO0VBQ2xDO0FBRUE7SUFDRSxtQkFBbUI7RUFDckI7QUFFQTtJQUNFLGVBQWU7SUFDZixnQkFBZ0I7RUFDbEI7QUFFQTtJQUNFLGlCQUFpQjtFQUNuQjtBQUVBO0lBQ0UsWUFBWTtJQUNaLGVBQWU7RUFDakI7QUFFQTtJQUNFLG9CQUFvQjtFQUN0QjtBQUVBO0lBQ0UsV0FBVztJQUNYLGFBQWE7SUFDYixlQUFlO0lBQ2Ysb0JBQW9CO0VBQ3RCO0FBRUE7O2tFQUVnRTtBQUNoRSxtREFBbUQ7QUFDbkQ7SUFDRTtNQUNFLHNCQUFzQjtNQUN0QixXQUFXO01BQ1gsYUFBYTtNQUNiLGtCQUFrQjtNQUNsQixTQUFTO01BQ1QsV0FBVztNQUNYLGtCQUFrQjtNQUNsQixZQUFZO01BR1osZ0NBQWdDO0lBQ2xDOztJQUVBO01BQ0UsV0FBVztNQUNYLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIsV0FBVztNQUNYLGFBQWE7TUFDYixzQkFBc0I7SUFDeEI7O0lBRUE7TUFDRSxlQUFlO01BQ2YsY0FBYztJQUNoQjs7SUFFQTtNQUNFLFdBQVc7TUFDWCxXQUFXO01BQ1gsWUFBWTtNQUNaLGFBQWE7TUFDYixzQkFBc0I7TUFDdEIsMkJBQTJCO01BQzNCLGtCQUFrQjtNQUNsQixtQkFBbUI7SUFDckI7O0lBRUE7TUFDRSxXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLFdBQVc7TUFDWCxnQkFBZ0I7SUFDbEI7O0lBRUE7TUFDRSxXQUFXO01BQ1gsa0JBQWtCO0lBQ3BCOztJQUVBO01BQ0UsV0FBVztNQUNYLFlBQVk7TUFDWixjQUFjO01BQ2QsZ0JBQWdCO0lBQ2xCOztJQUVBO01BQ0UsV0FBVztNQUNYLFdBQVc7TUFDWCx5QkFBeUI7TUFDekIsY0FBYztNQUNkLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxXQUFXO01BQ1gseUJBQXlCO01BQ3pCLGtCQUFrQjtNQUNsQixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLGVBQWU7TUFDZix5QkFBeUI7TUFDekIsb0JBQW9CO0lBQ3RCOztJQUVBO01BQ0UsV0FBVztNQUNYLGdCQUFnQjtJQUNsQjs7SUFFQTtNQUNFLGdCQUFnQjtJQUNsQjs7SUFFQTtNQUNFLFdBQVc7SUFDYjs7SUFFQTtNQUNFLFlBQVk7SUFDZDs7SUFFQTtNQUNFLFVBQVU7SUFDWjs7SUFFQTtNQUNFLGtCQUFrQjtJQUNwQjtFQUNGO0FBRUE7SUFDRTtNQUNFLFdBQVc7SUFDYjs7SUFFQTtNQUNFLGdCQUFnQjtJQUNsQjs7SUFFQTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxZQUFZO0lBQ2Q7O0lBRUE7TUFDRSxVQUFVO01BQ1YsaUJBQWlCO0lBQ25COztJQUVBO01BQ0UsaUJBQWlCO01BQ2pCLFNBQVM7TUFDVCxtQkFBbUI7SUFDckI7O0lBRUE7TUFDRSxXQUFXO0lBQ2I7O0lBRUE7TUFDRSxZQUFZO0lBQ2Q7RUFDRjtBQUVBO0lBQ0U7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsV0FBVztJQUNiOztJQUVBO01BQ0UsU0FBUztJQUNYOztJQUVBO01BQ0UsVUFBVTtNQUNWLGdCQUFnQjtJQUNsQjtFQUNGO0FBRUE7SUFDRTtNQUNFLGtCQUFrQjtJQUNwQjs7SUFFQTtNQUNFLGtCQUFrQjtNQUNsQixnQkFBZ0I7SUFDbEI7RUFDRjtBQUVBO0lBQ0U7TUFDRSxlQUFlO0lBQ2pCOztJQUVBO01BQ0UsVUFBVTtJQUNaOztJQUVBO01BQ0UsWUFBWTtJQUNkOztJQUVBO01BQ0UsVUFBVTtNQUNWLGNBQWM7TUFDZCxnQkFBZ0I7SUFDbEI7O0lBRUE7TUFDRSxVQUFVO01BQ1YsZUFBZTtNQUNmLGdCQUFnQjtJQUNsQjtFQUNGO0FBRUE7SUFDRTtNQUNFLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxXQUFXO01BQ1gsVUFBVTtJQUNaOztJQUVBO01BQ0UsZ0JBQWdCO01BQ2hCLGVBQWU7SUFDakI7O0lBRUE7TUFDRSxZQUFZO0lBQ2Q7O0lBRUE7TUFDRSxVQUFVO0lBQ1o7RUFDRjtBQUVBO0lBQ0U7TUFDRSxTQUFTO0lBQ1g7O0lBRUE7TUFDRSxZQUFZO0lBQ2Q7O0lBRUE7TUFDRSxVQUFVO0lBQ1o7O0lBRUE7TUFDRSxVQUFVO0lBQ1o7O0lBRUE7TUFDRSxVQUFVO01BQ1YsZUFBZTtNQUNmLGtCQUFrQjtJQUNwQjs7SUFFQTtNQUNFLGFBQWE7SUFDZjtFQUNGO0FBSUEsWUFBWTtBQUNaO01BQ0ksaUJBQWlCO01BQ2pCLCtCQUErQjtNQUMvQixnQkFBZ0I7TUFDaEI7RUFDSjtBQUNBO0lBQ0UscUJBQXFCO0VBQ3ZCO0FBQ0E7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtFQUN6QjtBQUNBO0lBQ0UsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtFQUNwQjtBQUNBO0lBQ0UsZUFBZTtJQUNmLGNBQWM7SUFDZCxnQkFBZ0I7RUFDbEI7QUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7QUFDQTtJQUNFLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YsY0FBYztFQUNoQjtBQUNBO0lBQ0UsVUFBVTtJQUNWO0VBQ0Y7QUFDQTtJQUNFLDBCQUEwQjtJQUMxQixlQUFlO0lBQ2YsY0FBYztFQUNoQjtBQUNBO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2IscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUTtJQUNwQyxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLDhCQUE4QjtFQUNoQyIsImZpbGUiOiJwcm9wZXJ0eS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgMS4gR2VuZXJhbCBTdHlsZXMgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4qIHtcclxuICAvKiBtYXJnaW46IDAgYXV0bzsgKi9cclxuICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG5odG1sIHtcclxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbiosXHJcbio6YmVmb3JlLFxyXG4qOmFmdGVyIHtcclxuICAtd2Via2l0LWJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgLW1vei1ib3gtc2l6aW5nOiBpbmhlcml0O1xyXG4gIGJveC1zaXppbmc6IGluaGVyaXQ7XHJcbn1cclxuXHJcbmEge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgY29sb3I6ICMxRjM3M0Q7XHJcbn1cclxuXHJcbmJvZHkge1xyXG4gIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xyXG4gIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7XHJcbiAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcclxuICB0ZXh0LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAuMDEpIDAgMCAxcHg7XHJcbn1cclxuXHJcbmE6Zm9jdXMsXHJcbmE6aG92ZXIge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBjb2xvcjogI2ZmZDAwMDtcclxufVxyXG5cclxuaW1nIHtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG59XHJcblxyXG46OnNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOjotbW96LXNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOjotd2Via2l0LXNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOmFjdGl2ZSxcclxuOmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWxlZnQge1xyXG4gIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtcmlnaHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWNlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5sZWZ0IHtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLnJpZ2h0IHtcclxuICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5jZW50ZXIge1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG59XHJcblxyXG4uZmxleC1jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLnVwcGVyY2FzZSB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuLndoaXRlLXRleHQge1xyXG4gIGNvbG9yOiAjRkZGO1xyXG59XHJcblxyXG4ubGlnaHQtYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi55ZWxsb3ctYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmQwMDA7XHJcbn1cclxuXHJcbi5wb2ludGVyIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5oaWRlIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uc2hvdyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5zbGlkZSB7XHJcbiAgbGVmdDogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuXHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDIuIFR5cG9ncmFwaHkgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmJvZHkge1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxM3B0O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMXM7XHJcbn1cclxuXHJcbmgxLFxyXG4uaDEge1xyXG4gIGZvbnQtc2l6ZTogNThweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5oMixcclxuLmgyIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuaDMsXHJcbi5oMyB7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbmg0LFxyXG4uaDQge1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5oNSxcclxuLmg1IHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBjb2xvcjogIzkxOUVCMTtcclxufVxyXG5cclxuaDYsXHJcbi5oNiB7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG59XHJcblxyXG5wIHtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDEuODtcclxuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjAyNWVtO1xyXG59XHJcblxyXG5saSB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDI1ZW07XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG59XHJcblxyXG4uYW5pbWF0ZSB7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4ubHV4LXNoYWRvdyB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCAxNXB4IDBweCByZ2JhKDI5LCAzMSwgMzYsIDAuMSk7XHJcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMTBweCAxNXB4IDBweCByZ2JhKDI5LCAzMSwgMzYsIDAuMSk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDEwcHggMTVweCAwcHggcmdiYSgyOSwgMzEsIDM2LCAwLjEpO1xyXG59XHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAzLiBIZWFkZXJcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmhlYWRlciB7XHJcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbn1cclxuXHJcbmhlYWRlciBuYXYge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogOTVweDtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogOTk5OTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4uc21hbGwtbmF2IHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4ubG9nbyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICBmb250LXNpemU6IDE4cHQ7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgLyogaGVpZ2h0OiA5MHB4OyAqL1xyXG59XHJcblxyXG4ubWVudSB7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbn1cclxuXHJcbi5wYWdlLW1lbnUge1xyXG4gIHdpZHRoOiA3MCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLnJlZ2lzdHJhdGlvbiB7XHJcbiAgd2lkdGg6IDMwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbn1cclxuXHJcbi5qb2luLXVzIHtcclxuICB3aWR0aDogNDAlO1xyXG4gIGhlaWdodDogOTVweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG59XHJcblxyXG4uZ2V0dGluZy1zdGFydGVkIHtcclxuICB3aWR0aDogNjAlO1xyXG4gIGhlaWdodDogOTVweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufVxyXG5cclxuLm1haW4tYnRuIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIHBhZGRpbmc6IDEwcHggMTBweDtcclxuICBtYXJnaW46IDBweCA1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZmZkMDAwO1xyXG4gIHRyYW5zaXRpb246IDAuNHMgYWxsO1xyXG59XHJcblxyXG4ubWFpbi1idG46aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmQwMDA7XHJcbn1cclxuXHJcbi5tYWluLWJ0bjpob3ZlciBhIHtcclxuICBjb2xvcjogIzFGMzczRDtcclxufVxyXG5cclxuLm1haW4tYnRuIGEge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4ucGFnZS1tZW51IGxpIHtcclxuICBwYWRkaW5nOiA4cHg7XHJcbn1cclxuXHJcbi5hY3RpdmUge1xyXG4gIGNvbG9yOiAjZmZkMDAwO1xyXG59XHJcblxyXG4vKi0tIEhlcm8gQXJlYSAtLSovXHJcbi5oZXJvIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcwMHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuLnRpdGxlIHtcclxuICB3aWR0aDogNjAlO1xyXG4gIGhlaWdodDogMzB2aDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogOTk5O1xyXG4gIHRvcDogMjN2aDtcclxufVxyXG5cclxuLmhlcm8taW1hZ2Uge1xyXG4gIHdpZHRoOiA4OCU7XHJcbiAgaGVpZ2h0OiA2MzBweDtcclxuICBmbG9hdDogcmlnaHQ7XHJcblxyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltYWdlcy9oZXJvLnBuZycpO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHJpZ2h0OiAtNjBweDtcclxufVxyXG5cclxuLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGJvdHRvbTogMDtcclxufVxyXG5cclxuLmhlcm8taW1hZ2UtaW5mbyBwIHtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG5cclxuLmluZm8ge1xyXG4gIG1hcmdpbjogNXB4IDM1cHg7XHJcbn1cclxuXHJcbi5pbmZvIHAge1xyXG4gIGNvbG9yOiAjZmZmXHJcbn1cclxuXHJcbi5zZWFyY2gge1xyXG4gIG1hcmdpbi10b3A6IDQwcHg7XHJcbiAgd2lkdGg6IDUwMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG5cclxuLnNlYXJjaCBpbnB1dCB7XHJcbiAgd2lkdGg6IDc1JTtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2VhcmNoLnN2ZycpIG5vLXJlcGVhdCA4cHggOHB4O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQ7XHJcbiAgYm9yZGVyOiAwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBwYWRkaW5nOiAyNXB4IDQwcHg7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA1cHg7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCAxNXB4IHRvcCAyOXB4O1xyXG59XHJcblxyXG4uc2VhcmNoLWJ0biB7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBib3JkZXI6IDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHBhZGRpbmc6IDI1cHggMTBweDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5zZWFyY2gtYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlciB7XHJcbiAgY29sb3I6ICM5MTlFQjE7XHJcbn1cclxuXHJcbi5oZXJvLXdyYXBwZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcyNXB4O1xyXG4gIHBhZGRpbmctdG9wOiA5NXB4O1xyXG59XHJcblxyXG4uc2xpZGUtZG93biB7XHJcbiAgd2lkdGg6IDIwcHg7XHJcbiAgaGVpZ2h0OiA2NXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDA7XHJcblxyXG59XHJcblxyXG4uc2xpZGUtZG93biBwIHtcclxuICB0cmFuc2Zvcm06IHJvdGF0ZSgtOTBkZWcpO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGNvbG9yOiAjOTE5RUIxO1xyXG59XHJcblxyXG5cclxuXHJcbi8qIFByb3BlcnR5ICovXHJcbi5wcm9wZXJ0eSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjZGNUY0O1xyXG4gICAgcGFkZGluZzogNjBweCAwcHggNzBweCAwcHg7XHJcbn1cclxuXHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICA5LiBGb290ZXJcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmZvb3RlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzMzMzO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDY1cHggMHB4O1xyXG4gIH1cclxuICBcclxuICAuZm9vdGVyLXRvcCB7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzkxOUVCMTtcclxuICAgIHBhZGRpbmctYm90dG9tOiA0NXB4O1xyXG4gIH1cclxuICBcclxuICAuTmV3c2xldHRlciBpbnB1dCB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgcGFkZGluZzogMHB4IDE1cHg7XHJcbiAgICBib3JkZXI6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDI2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5uZXdzbGV0dGVyLWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJmYmZiO1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJvcmRlcjogMHB4O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2Fycm93LXJpZ2h0LnN2ZycpO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDI2cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5uZXdzbGV0dGVyLWJ0bjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICB9XHJcbiAgXHJcbiAgLmZvb3Rlci1sb2dvIGg0IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDYwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5mb290ZXItbG9nbyBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5mb290ZXItYm90dG9tIHtcclxuICAgIHBhZGRpbmctdG9wOiA3MHB4O1xyXG4gIH1cclxuICBcclxuICAuZm9vdGVyLWJvdHRvbSBhIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICB9XHJcbiAgXHJcbiAgLmZvb3Rlci1jb2x1bW4gaDUge1xyXG4gICAgcGFkZGluZy1ib3R0b206IDM1cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5mb290ZXItY29sdW1uIGEge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDE4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIC8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgICAgICAxMC4gUkVTUE9OU0lWRVxyXG4gICAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbiAgLyogTGFyZ2UgZGV2aWNlcyAobGFwdG9wcy9kZXNrdG9wcywgOTkycHggYW5kIHVwKSAqL1xyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAgIC5tZW51IHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgdG9wOiA3MHB4O1xyXG4gICAgICByaWdodDogMTAwJTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICB6LWluZGV4OiA5OTk7XHJcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgIHRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnBhZ2UtbWVudSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBoZWlnaHQ6IDUwdmg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgZmxvYXQ6IG5vbmU7XHJcbiAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcbiAgXHJcbiAgICAucGFnZS1tZW51IGxpIHtcclxuICAgICAgZm9udC1zaXplOiAxNXB0O1xyXG4gICAgICBwYWRkaW5nOiAxLjV2aDtcclxuICAgIH1cclxuICBcclxuICAgIC5yZWdpc3RyYXRpb24ge1xyXG4gICAgICBmbG9hdDogbm9uZTtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGhlaWdodDogNTB2aDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiA1dmg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuam9pbi11cyB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGhlaWdodDogMHB4O1xyXG4gICAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmpvaW4tdXMgbGkge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLm1lbnUtYnV0dG9uIHtcclxuICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbi10b3A6IDI2cHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAubWVudS1idXR0b24gc3BhbiB7XHJcbiAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICBoZWlnaHQ6IDVweDtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzFGMzczRDtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbjogNnB4IDBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5tYWluLWJ0biB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gICAgICBwYWRkaW5nOiAxMHB4IDEwcHg7XHJcbiAgICAgIG1hcmdpbjogMHB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDAwMDtcclxuICAgICAgdHJhbnNpdGlvbjogMC40cyBhbGw7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc3RhdGlzdGljLWJveDpudGgtY2hpbGQoMSkge1xyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5zdGF0aXN0aWMtYm94Om50aC1jaGlsZCgyKSB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc3RhdGlzdGljLWJveDpudGgtY2hpbGQoMykge1xyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgIH1cclxuICBcclxuICAgIC5zZWFyY2gtYXBwYXJ0bWVudHMge1xyXG4gICAgICB3aWR0aDogMzAwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgICB3aWR0aDogMzAlO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmhvdy1pdC13b3JrcyB7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gICAgLmhlcm8taW1hZ2Uge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICBcclxuICAgIC5pbmZvIHtcclxuICAgICAgbWFyZ2luOiAwcHggMTBweDtcclxuICAgIH1cclxuICBcclxuICAgIGgxIHtcclxuICAgICAgZm9udC1zaXplOiA0NXB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgICAgIHdpZHRoOiAyNzBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICAgIHdpZHRoOiAzNyU7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLndvcmstc2VydmljZS1pbWFnZSB7XHJcbiAgICAgIG1heC1oZWlnaHQ6IDYzMHB4O1xyXG4gICAgICBsZWZ0OiAwcHg7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuZm9vdGVyLWxvZ28gaDQge1xyXG4gICAgICBmbG9hdDogbGVmdDtcclxuICAgIH1cclxuICBcclxuICAgIC5mb290ZXItbG9nbyBwIHtcclxuICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDY1MHB4KSB7XHJcbiAgICAuaW5mbyBwIHtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgICAgIHJpZ2h0OiA0MHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnRpdGxlIHtcclxuICAgICAgdG9wOiAxN3ZoO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaC1hbGwtYnRuIHtcclxuICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1NzVweCkge1xyXG4gICAgLmZvb3Rlci1ib3R0b20ge1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuZm9vdGVyLWNvbHVtbiBhIHtcclxuICAgICAgZGlzcGxheTogbGlzdC1pdGVtO1xyXG4gICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU0MHB4KSB7XHJcbiAgICAuaW5mbyBwIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2VhcmNoIHtcclxuICAgICAgd2lkdGg6IDQwMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaC1hbGwtYnRuIHtcclxuICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgbWFyZ2luOiAwcHggMTAlO1xyXG4gICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ3MHB4KSB7XHJcbiAgICAuaW5mbyBwIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgICAgIHJpZ2h0OiAzMHB4O1xyXG4gICAgICB3aWR0aDogOTElO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmhlcm8taW1hZ2UtaW5mbyBwIHtcclxuICAgICAgcGFkZGluZzogNXB4IDVweDtcclxuICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaCB7XHJcbiAgICAgIHdpZHRoOiAzMDBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzgwcHgpIHtcclxuICAgIC50aXRsZSB7XHJcbiAgICAgIHRvcDogMjJ2aDtcclxuICAgIH1cclxuICBcclxuICAgIC5zZWFyY2gge1xyXG4gICAgICB3aWR0aDogMjUwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2VhcmNoLWJ0biB7XHJcbiAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLnNlYXJjaCBpbnB1dCB7XHJcbiAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgcGFkZGluZzogMjdweCA0MHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmluZm8gaW1nIHtcclxuICAgICAgZGlzcGxheTogbm9uZTtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcblxyXG5cclxuICAvKiBEZXRhaWxzICovXHJcbiAgLmRldGFpbHN7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3OyAqL1xyXG4gICAgICBtYXgtd2lkdGg6IDMwMHB4O1xyXG4gICAgICBwYWRkaW5nOiAyMHB4XHJcbiAgfVxyXG4gIC5kZXRhaWxzIHAge1xyXG4gICAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xyXG4gIH1cclxuICAuZmxleC1jZW50ZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbiAgLnByaWNlIHtcclxuICAgIC8qIHdpZHRoOiA1MCU7ICovXHJcbiAgICAvKiBoZWlnaHQ6IDEwMCU7ICovXHJcbiAgfVxyXG4gIC5wcmljZSBwIHtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGNvbG9yOiAjZmZkMDAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICB9XHJcbiAgLmJhdGhyb29tcyB7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuICAuYmF0aHJvb21zIHAge1xyXG4gICAgcGFkZGluZzogMTVweCAwcHggMHB4IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBjb2xvcjogIzkxOUVCMTtcclxuICB9XHJcbiAgLmJlZHJvb21zIHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCVcclxuICB9XHJcbiAgLmJlZHJvb21zIHAge1xyXG4gICAgcGFkZGluZzogMTVweCAwcHggMHB4IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBjb2xvcjogIzkxOUVCMTtcclxuICB9XHJcbiAgLmFycm93c3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIC8qIHBvc2l0aW9uOnN0YXRpYzsgKi9cclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDsgbGVmdDogMDsgYm90dG9tOiAwOyByaWdodDogMDtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PropertyComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-property',
                templateUrl: './property.component.html',
                styleUrls: ['./property.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }]; }, null); })();


/***/ }),

/***/ "84zG":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../footer/footer.component */ "fp1T");




class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
}
AboutComponent.ɵfac = function AboutComponent_Factory(t) { return new (t || AboutComponent)(); };
AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AboutComponent, selectors: [["app-about"]], decls: 55, vars: 0, consts: [["id", "home"], [1, "animate"], [1, "container"], [1, "logo", "left"], ["routerLink", "/"], ["src", "../../assets/images/logo_.png", 1, "logo"], [1, "menu-button", "hide", "right", "pointer"], [1, "menu", "left"], [1, "page-menu", "left"], [1, "registration", "flex-center"], [1, "join-us"], [1, "pointer", "animate"], ["href", "#contact"], [1, "getting-started"], ["routerLink", "/", 1, "main-btn", "pointer", "text-center", "animate"], [1, "property"], [1, "row"], [1, "col-12", "col-lg-12"], [2, "margin-top", "60px"]], template: function AboutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h1", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "About us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "16th August 85 Villa was registered in 2019. The Company was formed to develop well designed, competitively priced real estate in Ghana. Over the past years we\u2019ve been consistent in our objective of contributing our part to the growth and development of Ghana\u2019s real estate. 16th August Villa is a finest focused property company with a large, well balanced portfolio comprised of residential, commercial and mixed-use properties. It is well-known for its impressive development of residential apartments.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "The Company also has a small but growing portfolio of property investments in Ghana and an interest in property development across borders. We have also served as a residential and industrial Property Management Specialists; where we\u2019ve gained good reputation in property management; because we deliver a service that provides integrity, professionalism and peace of mind to our clients.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Find homes for sale, rent, commercial activities and vacation at different locations all in one place. This way, you can be sure that you will find your dream home, no matter how hidden it is. Easy, convenient and fast. That\u2019s our promise.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "16TH AUGUST 85, VILLA, and Homes allow you to filter your search results to find properties that are relevant to you. You can select the city, neighborhood, price, size, number of rooms and bathrooms and a long (seriously, long) list of other criteria.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "When you choose the app, 16th August Villa Homes keeps working for you. Just create an alert (or several) and we will send you a notification when there\u2019s a new ad that matches your search criteria.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Just type your search: house, property, flat, rental, vacation or even offices. You can look for as many listings as you want, and create as many alerts as you want.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Our Core Values");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Integrity:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " We embrace the highest standards of ethical behavior and transparency in every aspect of our business to yield a company that is trusted by its clients and stakeholders.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Excellence:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, " Our commitment to professionalism and excellence ensures that our clients receive the highest quality service. We aspire to provide flawless execution and delivery of our products and services. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "b");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Innovation:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, " We thrive on creativity and originality. In today\u2019s fast paced technological climate, our ideas, concepts, and processes are essential to the continued success and growth of the company. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "app-footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__["FooterComponent"]], styles: ["h1[_ngcontent-%COMP%], h4[_ngcontent-%COMP%]{\r\n    color: #dbb300;\r\n}\r\na[_ngcontent-%COMP%] {\r\n    text-decoration: none !important;\r\n    position: relative !important;\r\n    color: #1F373D ;\r\n    \r\n    font-family: 'Montserrat', sans-serif\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFib3V0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxnQ0FBZ0M7SUFDaEMsNkJBQTZCO0lBQzdCLGVBQWU7SUFDZixzQkFBc0I7SUFDdEI7RUFDRiIsImZpbGUiOiJhYm91dC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDEsaDR7XHJcbiAgICBjb2xvcjogI2RiYjMwMDtcclxufVxyXG5hIHtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogIzFGMzczRCA7XHJcbiAgICAvKiBmb250LXdlaWdodDogNzAwOyAqL1xyXG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0Jywgc2Fucy1zZXJpZlxyXG4gIH0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-about',
                templateUrl: './about.component.html',
                styleUrls: ['./about.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "9vUh":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../footer/footer.component */ "fp1T");




const _c0 = function () { return ["/property", 0]; };
const _c1 = function () { return ["/property", 1]; };
const _c2 = function () { return ["/property", 2]; };
class HomeComponent {
    constructor() { }
    ngOnInit() {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        $(document).ready(function () {
            /* Check width on page load*/
            if ($(window).width() < 993) {
                $('.menu-button').addClass('hide');
            }
            else { }
        });
        $(window).resize(function () {
            /*If browser resized, check width again */
            if ($(window).width() < 993) {
                $('.menu-button').removeClass('hide');
            }
            else {
                $('.menu-button').addClass('hide');
            }
        });
        $(function () {
            var hasBeenTrigged = false;
            $(window).scroll(function () {
                if ($(this).scrollTop() >= 100 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
                    $('nav').addClass('small-nav');
                    hasBeenTrigged = true;
                }
                else if ($(this).scrollTop() <= 100 && hasBeenTrigged) {
                    $('nav').removeClass('small-nav');
                    hasBeenTrigged = false;
                }
            });
        });
        //Toogle Mobile Menu
        $(".menu-button").click(function () {
            $('.menu').toggleClass('slide');
        });
        // Scroll To Section of Page
        $(document).on('click', 'a[href^="#"]', function (e) {
            e.preventDefault();
            $('.menu').removeClass('slide');
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 800, 'linear');
        });
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 232, vars: 6, consts: [["id", "home"], [1, "animate"], [1, "container"], [1, "logo", "left"], ["href", "#home"], ["src", "../../assets/images/logo_.png", 1, "logo"], [1, "menu-button", "hide", "right", "pointer"], [1, "menu", "left"], [1, "page-menu", "left"], ["routerLink", "/about"], ["href", "#works"], ["href", "#sevices"], [1, "registration", "flex-center"], [1, "join-us"], [1, "pointer", "animate"], ["href", "#contact"], [1, "getting-started"], ["routerLink", "/all", 1, "main-btn", "pointer", "text-center", "animate"], [1, "hero"], [1, "hero-wrapper"], [1, "title"], ["id", "animated-background", 1, "hero-image"], [1, "hero-image-info", "yellow-bg", "flex-center"], [1, "info", "flex-center"], ["src", "../../assets/images/call-icon.svg", "alt", ""], ["src", "../../assets/images/point-icon.svg", "alt", ""], [1, "slide-down", "text-center", "pointer"], ["href", "#about"], ["src", "../../assets/images/scroll.svg", "alt", ""], ["id", "appartments", 1, "appartments"], [1, "row"], [1, "col-12"], [1, "col-12", "col-md-6", "col-lg-4"], [1, "appartment-box", 3, "routerLink"], [1, "appartment-image"], ["src", "../../assets/images/apartments/temp.jpg", "alt", ""], [1, "appartment-info"], [1, "appartment-title"], [1, "appartment-details"], [1, "price", "left"], [1, "bedrooms", "right", "flex-center"], ["src", "../../assets/images/bed.svg", "alt", "", 1, "left"], [1, "left"], [1, "bathrooms", "right", "flex-center"], ["src", "../../assets/images/shower.svg", "alt", "", 1, "left"], [1, "search", "lux-shadow", "search-appartments"], [1, "search-all"], ["routerLink", "/all", 1, "search-btn", "left", "search-all-btn"], ["id", "works", 1, "how-it-works"], [1, "col-12", "works-title"], [1, "col-12", "col-md-12", "col-lg-6"], [1, "work-box", "animate"], [1, "work-box-number"], [1, "work-box-title"], [1, "work-box-text"], [1, "work-box-link"], ["routerLink", "/all"], ["id", "sevices", 1, "row", "work-services"], [1, "col-12", "work-services-title"], [1, "col-12", "col-md-7"], [1, "work-service-image"], [1, "col-12", "col-md-5", "work-service-info"], ["id", "agents", 1, "agents"], [1, "row", "agents-services"], [1, "col-12", "agents-title"], [1, "appartment-box"], ["src", "../../assets/images/agents/2.jpg", "alt", ""], [1, "price", "left", "agent-name"], [1, "agent-social"], ["href", "#", 2, "font-size", "10px", "width", "100%", "flex-direction", "row"], ["src", "../../assets/images/envelope -black.svg", "alt", "", 2, "fill", "blue"], ["src", "../../assets/images/agents/1.jpg", "alt", ""], ["src", "../../assets/images/envelope -black.svg", "alt", ""]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "About");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "How It Works");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Sevices");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "li", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Properties for sale");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Comfort beyond ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, " you wildest dreams");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "(+233) 322 031 764");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Accra,Ghana");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Scroll Down");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "img", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "section", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Properties available");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "img", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "5 BA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](103, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "img", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "4.5 BA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "button", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "View All Properties");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "section", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "How it works?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "div", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "01");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "01");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, "Choose a property");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](145, "Simply choose any of our properties, you may be interested in from the section above. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "div", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "a", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "Properties");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "div", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, "02");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "02");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "p", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "Contact us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Contact us by call or email. Our agents are always on standby. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "div", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Contact us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "div", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "div", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Services");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "In this space market-facing, yet UI work flows , or bake it in. Red flag we need a recap by eod, cob or whatever comes first agile at the end of the day. Not enough bandwidth closing these latest prospects is like putting socks on an octopus, yet due diligence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "Develop");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "In this space market-facing, yet UI work flows , or bake it in. Cob or whatever comes first agile at the end of the day. Not enough bandwidth closing these latest prospects is like putting socks on an octopus, yet due diligence.Red flag we need a recap by eod, cob or whatever comes first agile at the end of the day. Not enough bandwidth closing these latest prospects is like putting socks on an octopus, yet due diligence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "Sell");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](184, "In this space market-facing, yet UI work flows , or bake it in. Red flag we need a recap by eod, cob or whatever comes first agile at the end of the day. Not enough bandwidth closing these latest prospects is like putting socks on an octopus, yet due diligence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, "Rent");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "In this space market-facing, yet UI work flows , or bake it in. Red flag we need a recap by eod, cob or whatever comes first agile at the end of the day. Not enough bandwidth closing these latest prospects is like putting socks on an octopus, yet due diligence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "section", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "div", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, "Meet the team");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](196, "In this space market-facing, yet UI work flows , or bake it in.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](197, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "Red flag we need a recap by eod, cob or whatever comes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](199, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](200, "first agile at the end of the day. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](201, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](205, "img", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](211, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "a", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](213, "img", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](214, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, " \u00A0\u00A0email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](216, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](217, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](219, "img", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](221, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](222, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](223, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](224, "Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](225, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](226, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "a", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](228, "img", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](230, "\u00A0\u00A0email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](231, "app-footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c2));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__["FooterComponent"]], styles: ["*[_ngcontent-%COMP%] {\r\n    \r\n    padding: 0;\r\n}\r\n\r\n\r\nhtml[_ngcontent-%COMP%] {\r\n    box-sizing: border-box;\r\n  }\r\n\r\n\r\n*[_ngcontent-%COMP%], *[_ngcontent-%COMP%]:before, *[_ngcontent-%COMP%]:after {\r\n    box-sizing: inherit;\r\n}\r\n\r\n\r\na[_ngcontent-%COMP%] {\r\n    text-decoration: none;\r\n    position: relative;\r\n    color: #1F373D;\r\n}\r\n\r\n\r\nbody[_ngcontent-%COMP%] {\r\n    -webkit-font-smoothing: antialiased;\r\n    -moz-osx-font-smoothing: grayscale;\r\n    text-rendering: optimizeLegibility;\r\n    text-shadow: rgba(0,0,0,.01) 0 0 1px;\r\n}\r\n\r\n\r\na[_ngcontent-%COMP%]:focus, a[_ngcontent-%COMP%]:hover {\r\n    text-decoration: none;\r\n    color: #ffd000;\r\n}\r\n\r\n\r\nimg[_ngcontent-%COMP%] {\r\n    max-width: 100%;\r\n    height: auto;\r\n}\r\n\r\n\r\n[_ngcontent-%COMP%]::selection {\r\n    background: #1F373D;\r\n    color: #fff;\r\n    text-shadow: none;\r\n}\r\n\r\n\r\n[_ngcontent-%COMP%]::-moz-selection {\r\n    background: #1F373D;\r\n    color: #fff;\r\n    text-shadow: none;\r\n}\r\n\r\n\r\n[_ngcontent-%COMP%]::-webkit-selection {\r\n    background: #1F373D;\r\n    color: #fff;\r\n    text-shadow: none;\r\n}\r\n\r\n\r\n[_ngcontent-%COMP%]:active, [_ngcontent-%COMP%]:focus {\r\n    outline: none !important;\r\n}\r\n\r\n\r\n.text-left[_ngcontent-%COMP%] {\r\n    text-align: left !important;\r\n}\r\n\r\n\r\n.text-right[_ngcontent-%COMP%] {\r\n    text-align: right !important;\r\n}\r\n\r\n\r\n.text-center[_ngcontent-%COMP%] {\r\n    text-align: center !important;\r\n}\r\n\r\n\r\n.left[_ngcontent-%COMP%] {\r\n  float: left;\r\n}\r\n\r\n\r\n.right[_ngcontent-%COMP%] {\r\n  float: right;\r\n}\r\n\r\n\r\n.center[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n}\r\n\r\n\r\n.flex-center[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n\r\n.uppercase[_ngcontent-%COMP%] {\r\n    text-transform: uppercase;\r\n}\r\n\r\n\r\n.white-text[_ngcontent-%COMP%] {\r\n    color: #FFF;\r\n}\r\n\r\n\r\n.light-bg[_ngcontent-%COMP%] {\r\n    background-color: #fff;\r\n}\r\n\r\n\r\n.yellow-bg[_ngcontent-%COMP%] {\r\n  background-color: #fdad00;\r\n}\r\n\r\n\r\n.pointer[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n}\r\n\r\n\r\n.hide[_ngcontent-%COMP%] {\r\n  display: none;\r\n}\r\n\r\n\r\n.show[_ngcontent-%COMP%] {\r\n  display: block;\r\n}\r\n\r\n\r\n.slide[_ngcontent-%COMP%] {\r\n  left: 0;\r\n  transition: all 0.4s ease-in-out;\r\n}\r\n\r\n\r\n\r\n\r\n\r\nbody[_ngcontent-%COMP%] {\r\n    color: #1F373D;\r\n    font-family: 'Montserrat', sans-serif;\r\n    font-size: 13pt;\r\n    font-weight: 400;\r\n    overflow-x: hidden;\r\n    transition: opacity 1s;\r\n}\r\n\r\n\r\nh1[_ngcontent-%COMP%], .h1[_ngcontent-%COMP%] {\r\n    font-size: 58px;\r\n    font-weight: 700;  \r\n  }\r\n\r\n\r\nh2[_ngcontent-%COMP%], .h2[_ngcontent-%COMP%] {\r\n    font-size: 40px;\r\n    font-weight: 700;  \r\n}\r\n\r\n\r\nh3[_ngcontent-%COMP%], .h3[_ngcontent-%COMP%] {\r\n    font-size: 40px;\r\n    font-weight: 700;  \r\n}\r\n\r\n\r\nh4[_ngcontent-%COMP%], .h4[_ngcontent-%COMP%] {\r\n    font-size: 25px;\r\n    font-weight: 700;  \r\n}\r\n\r\n\r\nh5[_ngcontent-%COMP%], .h5[_ngcontent-%COMP%] {\r\n    font-size: 14px;\r\n    font-weight: 400;\r\n    color: #919EB1;\r\n}\r\n\r\n\r\nh6[_ngcontent-%COMP%], .h6[_ngcontent-%COMP%] {\r\n    font-size: 14px;\r\n}\r\n\r\n\r\np[_ngcontent-%COMP%] {\r\n    font-size: 16px;\r\n    line-height: 1.8;\r\n    margin-bottom: 15px;\r\n    letter-spacing: 0.025em;\r\n}\r\n\r\n\r\nli[_ngcontent-%COMP%] {\r\n    font-size: 13px;\r\n    font-weight: 400;\r\n    letter-spacing: 0.025em;\r\n    display: inline;\r\n}\r\n\r\n\r\n.animate[_ngcontent-%COMP%] {\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n\r\n.lux-shadow[_ngcontent-%COMP%] {\r\n  box-shadow: 0px 10px 15px 0px rgba(29,31,36,0.1);\r\n}\r\n\r\n\r\n\r\n\r\n\r\nheader[_ngcontent-%COMP%] {\r\n  padding-bottom: 25px;\r\n}\r\n\r\n\r\nheader[_ngcontent-%COMP%]   nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 95px;\r\n  position: fixed;\r\n  z-index: 9999;\r\n  background-color: #fff;\r\n}\r\n\r\n\r\n.small-nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 70px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n\r\n.logo[_ngcontent-%COMP%] {\r\n  \r\n  \r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-start;\r\n  font-size: 18pt;\r\n  font-weight: 700;\r\n}\r\n\r\n\r\n.menu[_ngcontent-%COMP%] {\r\n  width: 80%;\r\n  height: 95px;\r\n}\r\n\r\n\r\n.page-menu[_ngcontent-%COMP%] {\r\n  width: 70%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n\r\n.registration[_ngcontent-%COMP%] {\r\n  width: 30%;\r\n  height: 95px;\r\n}\r\n\r\n\r\n.join-us[_ngcontent-%COMP%] {\r\n  width: 40%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n  padding-right: 20px;\r\n}\r\n\r\n\r\n.getting-started[_ngcontent-%COMP%] {\r\n  width: 60%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n}\r\n\r\n\r\n.main-btn[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  background-color: #ffd000;\r\n  padding: 10px 10px;\r\n  margin: 0px 5px;\r\n  border-radius: 5px;\r\n  font-size: 13px;\r\n  border: 1px solid #ffd000;\r\n  transition: 0.4s all;\r\n}\r\n\r\n\r\n.main-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: transparent;\r\n  color: #000000;\r\n  border: 1px solid #ffd000;\r\n}\r\n\r\n\r\n.main-btn[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n}\r\n\r\n\r\n.main-btn[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #fff;\r\n}\r\n\r\n\r\n.page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n  padding: 8px;\r\n}\r\n\r\n\r\n.active[_ngcontent-%COMP%] {\r\n  color: #ffd000;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.hero[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 700px;\r\n  position: relative;\r\n}\r\n\r\n\r\n.title[_ngcontent-%COMP%] {\r\n  width: 60%;\r\n  height: 30vh;\r\n  position: absolute;\r\n  z-index: 999;\r\n  top: 23vh;\r\n}\r\n\r\n\r\n.hero-image[_ngcontent-%COMP%] {\r\n  width: 88%;\r\n  height: 630px;\r\n  float: right;\r\n\r\n  \r\n  background-size: cover;\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  position: relative;\r\n  right: -60px;\r\n}\r\n\r\n\r\n#animated-background[_ngcontent-%COMP%]{\r\n\r\nbackground-size:cover;\r\n\r\nbackground-position:center;\r\n\r\nposition:relative;\r\n}\r\n\r\n\r\n@keyframes bgfade {\r\n0% {\r\nbackground-image: url('hero.png');\r\n}\r\n25% {\r\n\r\n}\r\n50% {\r\n\r\n}\r\n75% {\r\n\r\n}\r\n100% {\r\n\r\n}\r\n}\r\n\r\n\r\n\r\n\r\n\r\n#animated-background[_ngcontent-%COMP%]:before {\r\ncontent:\"\";\r\nposition:absolute;\r\ntop:0;\r\nbottom:0;\r\nleft:0;\r\nright:0;\r\nbackground-color:rgb(255, 255, 255);\r\nopacity:0.2;\r\n}\r\n\r\n\r\n.hero-image-info[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  right: 0;\r\n  bottom: 0;\r\n}\r\n\r\n\r\n.hero-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  font-size: 18px;\r\n  padding: 10px 20px;\r\n  margin-top: 15px;\r\n}\r\n\r\n\r\n.info[_ngcontent-%COMP%] {\r\n  margin: 5px 35px;\r\n}\r\n\r\n\r\n.info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  color: #fff\r\n\r\n}\r\n\r\n\r\n.search[_ngcontent-%COMP%] {\r\n  margin-top: 40px;\r\n  width: 500px;\r\n  position: absolute;\r\n}\r\n\r\n\r\n.search[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n  width: 75%;\r\n  background: url('search.svg') no-repeat 8px 8px;\r\n  background-position: left;\r\n  border: 0px;\r\n  background-color: #fff;\r\n  padding: 25px 40px;\r\n  font-size: 15px;\r\n  border-top-left-radius: 5px;\r\n  border-bottom-left-radius: 5px;\r\n  background-position: left 15px top 29px;\r\n}\r\n\r\n\r\n.search-btn[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  border: 0px;\r\n  background-color: #ffd000;\r\n  color: #fff;\r\n  padding: 25px 10px;\r\n  font-size: 15px;\r\n  font-weight: 700;\r\n  border-top-right-radius: 5px;\r\n  border-bottom-right-radius: 5px;\r\n}\r\n\r\n\r\n.search-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: #ffd000;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n\r\n[_ngcontent-%COMP%]::placeholder {\r\n  color: #919EB1;\r\n}\r\n\r\n\r\n.hero-wrapper[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  width: 100%;\r\n  height: 725px;\r\n  padding-top:95px;\r\n}\r\n\r\n\r\n.slide-down[_ngcontent-%COMP%] {\r\n  width:20px;\r\n  height: 65px;\r\n  position: absolute;\r\n  bottom: 0;\r\n\r\n}\r\n\r\n\r\n.slide-down[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  transform: rotate(-90deg);\r\n  font-size: 12px;\r\n  white-space: nowrap;\r\n  color: #919EB1;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.statistic[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  min-height: 650px;\r\n  padding: 145px 0px 80px 0px;\r\n}\r\n\r\n\r\n.statistic[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n padding:15px 0px 50px 0px;\r\n}\r\n\r\n\r\n.statistic[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n font-size: 13px;\r\n}\r\n\r\n\r\n.statistic-box[_ngcontent-%COMP%] {\r\n  background-color: #F6F5F4;\r\n  width: 36%;\r\n  height: 200px;\r\n  margin: 0px 0px 30px 30px;\r\n  padding-top: 40px;\r\n  float: right;\r\n}\r\n\r\n\r\n.statistic-box[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]:nth-child(1) {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n  color: #ffd000;\r\n  margin: 0px;\r\n  padding: 0px;\r\n}\r\n\r\n\r\n.statistic-box[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]:nth-child(2) {\r\n  font-size: 14px;\r\n  font-weight: 700;\r\n  color: #1F373D;\r\n  margin: 0px;\r\n  padding: 0px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.appartments[_ngcontent-%COMP%] {\r\n  background-color: #F6F5F4;\r\n  padding:60px 0px 70px 0px;\r\n}\r\n\r\n\r\n.appartments[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n\r\n.appartment-box[_ngcontent-%COMP%] {\r\n  box-shadow: 0px 2px 8px 0px rgba(24,49,56,0.15);\r\n  width: 100%;\r\n  height: 500px;\r\n  max-width: 350px;\r\n  margin: 0 auto;\r\n  margin-top: 35px;\r\n  cursor: pointer;\r\n  transition: box-shadow 200ms ease-out ;\r\n}\r\n\r\n\r\n.appartment-box[_ngcontent-%COMP%]:hover{\r\n  box-shadow: 1px 4px 10px rgba(136, 136, 136, 0.836);\r\n    transition: box-shadow 200ms ease-in;\r\n}\r\n\r\n\r\n.appartment-image[_ngcontent-%COMP%] {\r\n  height: 76%;\r\n}\r\n\r\n\r\n.appartment-image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n\r\n.appartment-info[_ngcontent-%COMP%] {\r\n  height: 25%;\r\n  background-color: #fff;\r\n  padding: 25px 20px;\r\n  \r\n}\r\n\r\n\r\n.appartment-title[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 50%;\r\n}\r\n\r\n\r\n.appartment-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 15px;\r\n  font-weight: 700;\r\n  letter-spacing: 0px;\r\n  line-height: 20px;\r\n}\r\n\r\n\r\n.appartment-details[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 50%;\r\n}\r\n\r\n\r\n.price[_ngcontent-%COMP%] {\r\n  width: 50%;\r\n  height: 100%;\r\n}\r\n\r\n\r\n.price[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 18px;\r\n  color: #ffd000;\r\n  font-weight: 700;\r\n}\r\n\r\n\r\n.bathrooms[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  height: 100%;\r\n}\r\n\r\n\r\n.bathrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 0px 10px;\r\n  font-size: 13px;\r\n  color: #919EB1;\r\n}\r\n\r\n\r\n.bedrooms[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  height: 100%\r\n}\r\n\r\n\r\n.bedrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 0px 10px;\r\n  font-size: 13px;\r\n  color: #919EB1;\r\n}\r\n\r\n\r\n.search-appartments[_ngcontent-%COMP%] {\r\n  position: inherit;\r\n}\r\n\r\n\r\n.search-all-btn[_ngcontent-%COMP%] {\r\n  margin-left: 20px;\r\n  border-radius: 5px;\r\n  background-color: #000;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.how-it-works[_ngcontent-%COMP%] {\r\n  background-color: #fff;\r\n  padding:60px 0px 20px 0px;\r\n}\r\n\r\n\r\n.works-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  margin-top: 20px;\r\n  line-height: 1.9rem;\r\n}\r\n\r\n\r\n.work-box[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 372px;\r\n  max-width: 345px;\r\n  margin: 0 auto;\r\n  margin-top: 60px;\r\n  overflow: hidden;\r\n  box-shadow: 0px 2px 8px 0px rgba(24,49,56,0.15);\r\n}\r\n\r\n\r\n.work-box-number[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 35%;\r\n  position: relative;\r\n}\r\n\r\n\r\n.work-box-number[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  top: -124px;\r\n  left: -45px;\r\n  font-size: 160px;\r\n  color: #1F373D;\r\n  font-weight: 700;\r\n}\r\n\r\n\r\n.work-box-title[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 15%;\r\n  padding: 0px 20px;\r\n}\r\n\r\n\r\n.work-box-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  color: #1F373D;\r\n}\r\n\r\n\r\n.work-box-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]:nth-child(1) {\r\n  margin-right: 15px;\r\n}\r\n\r\n\r\n.work-box-text[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 30%;\r\n  padding: 0px 20px 0px 55px;\r\n}\r\n\r\n\r\n.work-box-text[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n  font-size: 13px;\r\n  line-height: 2rem;\r\n  font-weight: 400;\r\n}\r\n\r\n\r\n.work-box-link[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 20%;\r\n  padding: 0px 10px 0px 55px;\r\n}\r\n\r\n\r\n.work-box-link[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n  font-size: 13px;\r\n  -webkit-text-decoration-line: underline;\r\n          text-decoration-line: underline;\r\n  font-weight: 700;\r\n}\r\n\r\n\r\n.work-box[_ngcontent-%COMP%]:hover {\r\n  background-color: #ffd000;\r\n}\r\n\r\n\r\n.work-box[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%]{\r\n  color: #ffffff;\r\n}\r\n\r\n\r\n.work-box[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%]{\r\n  color: #ffffff;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.work-services[_ngcontent-%COMP%] {\r\n  padding: 90px 0px 65px 0px;\r\n}\r\n\r\n\r\n.work-services-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n}\r\n\r\n\r\n.work-service-image[_ngcontent-%COMP%] {\r\n  background-color: aqua;\r\n  width: 100%;\r\n  height: 630px;\r\n  display: block;\r\n  background-image: url('services.png');\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  position: relative;\r\n  left: -105px;\r\n}\r\n\r\n\r\n.work-service-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 50px 0px;\r\n  font-size: 13px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.agents[_ngcontent-%COMP%] {\r\n  background-color: #F6F5F4;\r\n  padding-bottom: 90px;\r\n}\r\n\r\n\r\n.agents-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  margin-top: 20px;\r\n  line-height: 1.9rem;\r\n}\r\n\r\n\r\n.agent-name[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 15px;\r\n  color: #e9be01;\r\n}\r\n\r\n\r\n.agent-social[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  position: relative; \r\n  top: -5px;\r\n}\r\n\r\n\r\n.agent-social[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  \r\n}\r\n\r\n\r\n.agent-social[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:nth-child(2) {\r\n  \r\n}\r\n\r\n\r\n.appartment-info[_ngcontent-%COMP%] {\r\n  height: 24%;\r\n  background-color: #fff;\r\n  padding: 25px 20px;\r\n}\r\n\r\n\r\n.agents-services[_ngcontent-%COMP%] {\r\n  padding: 90px 0px 35px 0px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.add[_ngcontent-%COMP%] {\r\n  padding: 90px 0px;\r\n  background-color: #fff;\r\n}\r\n\r\n\r\n.add-image[_ngcontent-%COMP%] {\r\n  width: 540px;\r\n  height: 330px;\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n  position: relative;\r\n  margin-bottom: 80px;\r\n}\r\n\r\n\r\n.add1[_ngcontent-%COMP%] {\r\n  \r\n}\r\n\r\n\r\n.add2[_ngcontent-%COMP%] {\r\n  \r\n}\r\n\r\n\r\n.add3[_ngcontent-%COMP%] {\r\n  \r\n}\r\n\r\n\r\n.add-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 50px 0px;\r\n  font-size: 13px;\r\n  line-height: 1.8rem;\r\n}\r\n\r\n\r\n.add-image-info[_ngcontent-%COMP%] {\r\n  background-color: #ffd000;\r\n  width: 60%;\r\n  margin: 0px 20%;\r\n  height: 80px;\r\n  position: absolute;\r\n  bottom: -30px;\r\n}\r\n\r\n\r\n.add-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 0px 20px;\r\n  font-size: 18px;\r\n  font-weight: 700;\r\n  color: #fff;\r\n}\r\n\r\n\r\n.add2-info[_ngcontent-%COMP%] {\r\n  text-align: right;\r\n}\r\n\r\n\r\n.add-image-info2[_ngcontent-%COMP%] {\r\n  left: 150px;\r\n}\r\n\r\n\r\n.show-more-btn[_ngcontent-%COMP%] {\r\n  width: 70%;\r\n  border-radius: 0px;\r\n  background-color: #1F373D;\r\n  color: #fff;\r\n}\r\n\r\n\r\n.add3-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding-bottom: 10px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\nfooter[_ngcontent-%COMP%] {\r\n  background-color: #333333;\r\n  width: 100%;\r\n  color: #fff;\r\n  padding: 65px 0px;\r\n}\r\n\r\n\r\n.footer-top[_ngcontent-%COMP%] {\r\n  border-bottom: 1px solid #919EB1;\r\n  padding-bottom: 45px;\r\n}\r\n\r\n\r\n.Newsletter[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n  width: 80%;\r\n  height: 70px;\r\n  padding: 0px 15px;\r\n  border: 0px;\r\n  margin-top: 26px;\r\n}\r\n\r\n\r\n.newsletter-btn[_ngcontent-%COMP%] {\r\n  background-color: #fbfbfb;\r\n  width: 20%;\r\n  height: 70px;\r\n  border: 0px;\r\n  background-image: url('arrow-right.svg');\r\n  background-repeat: no-repeat;\r\n  background-position: center;\r\n  margin-top: 26px;\r\n}\r\n\r\n\r\n.newsletter-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: #ffd000;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n\r\n.footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n  margin-bottom:60px;\r\n}\r\n\r\n\r\n.footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  font-weight: 400;\r\n}\r\n\r\n\r\n.footer-bottom[_ngcontent-%COMP%] {\r\n  padding-top: 70px;\r\n}\r\n\r\n\r\n.footer-bottom[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\r\n color: white;\r\n font-size: 15px;\r\n}\r\n\r\n\r\n.footer-column[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%] {\r\n  padding-bottom: 35px;\r\n}\r\n\r\n\r\n.footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #fff;\r\n  display: flex;\r\n  font-size: 13px;\r\n  padding-bottom: 18px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n@media only screen and (max-width: 992px) {\r\n  .menu[_ngcontent-%COMP%] {\r\n    background-color: #fff;\r\n    width: 100%;\r\n    height: 100vh;\r\n    position: absolute;\r\n    top: 70px;\r\n    right: 100%;\r\n    text-align: center;\r\n    z-index: 999;\r\n    transition: all 0.4s ease-in-out;\r\n  }\r\n  .page-menu[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    height: 50vh;\r\n    text-align: center;\r\n    float: none;  \r\n    display: flex;\r\n    flex-direction: column;\r\n  }\r\n  .page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    font-size: 15pt;\r\n    padding: 1.5vh;\r\n  }\r\n  .registration[_ngcontent-%COMP%] {\r\n    float: none;\r\n    width: 100%;\r\n    height: 50vh;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    text-align: center;\r\n    padding-bottom: 5vh;\r\n  }\r\n  .join-us[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    text-align: center;\r\n    height: 0px;\r\n    margin-top: 30px;\r\n  }\r\n  .join-us[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    text-align: center;\r\n  }\r\n  .menu-button[_ngcontent-%COMP%] {\r\n    width: 40px;\r\n    height: 40px;\r\n    display: block;\r\n    margin-top: 26px;\r\n  }\r\n  .menu-button[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\r\n    width: 40px;\r\n    height: 5px;\r\n    background-color: #1F373D;\r\n    display: block;\r\n    margin: 6px 0px;\r\n  }\r\n  .main-btn[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    background-color: #ffd000;\r\n    padding: 10px 10px;\r\n    margin: 0px;\r\n    border-radius: 5px;\r\n    font-size: 13px;\r\n    border: 1px solid #ffd000;\r\n    transition: 0.4s all;\r\n  }\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(1) {\r\n    float: left;\r\n    margin-top:50px;\r\n  }\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(2) {\r\n    margin-top:50px;\r\n  }\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(3) {\r\n    float: left;\r\n  }\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 300px;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 30%;\r\n  }\r\n  .how-it-works[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 768px) {\r\n  .hero-image[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n  }\r\n  .info[_ngcontent-%COMP%] {\r\n    margin: 0px 10px;\r\n  }\r\n  h1[_ngcontent-%COMP%] {\r\n    font-size: 45px;\r\n  }\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 270px;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 37%;\r\n    margin-left: 10px;\r\n  }\r\n  .work-service-image[_ngcontent-%COMP%] {\r\n    max-height: 630px;\r\n    left: 0px;\r\n    margin-bottom: 50px;\r\n  }\r\n  .footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n    float: left;\r\n  }\r\n  .footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    float: right;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 650px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 14px;\r\n  }\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    right: 40px;\r\n  }\r\n  .title[_ngcontent-%COMP%] {\r\n    top: 17vh;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 40%;\r\n    margin-left: 5px;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 575px) {\r\n  .footer-bottom[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n  }\r\n  .footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    display: list-item;\r\n    list-style: none;\r\n  }\r\n  .footer-top[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 540px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 12px;\r\n  }\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    width: 90%;\r\n  }\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 400px;\r\n  }\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n    margin: 0 auto;\r\n    margin-top: 30px;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n    margin: 0px 10%;\r\n    margin-top: 15px;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 470px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 12px;\r\n  }\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    right: 30px;\r\n    width: 91%;\r\n  }\r\n  .hero-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    padding: 5px 5px;\r\n    font-size: 11px;\r\n  }\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 300px;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n  }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 380px) {\r\n  .title[_ngcontent-%COMP%] {\r\n    top: 22vh;\r\n  }\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 250px;\r\n  }\r\n  .search-btn[_ngcontent-%COMP%] {\r\n    width: 30%;\r\n  }\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n  }\r\n  .search[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n    width: 70%;\r\n    font-size: 12px;\r\n    padding: 27px 40px;\r\n  }\r\n  .info[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n    display: none;\r\n  }\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.logo[_ngcontent-%COMP%]{\r\n  width: 120px;\r\n  \r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs4REFPOEQ7OztBQUc5RDs7Ozs7Ozs7Ozs7OERBVzhEOzs7QUFFOUQ7OzhEQUU4RDs7O0FBQzlEO0lBQ0ksb0JBQW9CO0lBQ3BCLFVBQVU7QUFDZDs7O0FBQ0E7SUFHSSxzQkFBc0I7RUFDeEI7OztBQUNGO0lBR0ksbUJBQW1CO0FBQ3ZCOzs7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsY0FBYztBQUNsQjs7O0FBQ0E7SUFDSSxtQ0FBbUM7SUFDbkMsa0NBQWtDO0lBQ2xDLGtDQUFrQztJQUNsQyxvQ0FBb0M7QUFDeEM7OztBQUNBOztJQUVJLHFCQUFxQjtJQUNyQixjQUFjO0FBQ2xCOzs7QUFDQTtJQUNJLGVBQWU7SUFDZixZQUFZO0FBQ2hCOzs7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsaUJBQWlCO0FBQ3JCOzs7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsaUJBQWlCO0FBQ3JCOzs7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsaUJBQWlCO0FBQ3JCOzs7QUFDQTs7SUFFSSx3QkFBd0I7QUFDNUI7OztBQUNBO0lBQ0ksMkJBQTJCO0FBQy9COzs7QUFFQTtJQUNJLDRCQUE0QjtBQUNoQzs7O0FBRUE7SUFDSSw2QkFBNkI7QUFDakM7OztBQUNBO0VBQ0UsV0FBVztBQUNiOzs7QUFDQTtFQUNFLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSxjQUFjO0FBQ2hCOzs7QUFDQTtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCOzs7QUFDQTtJQUNJLHlCQUF5QjtBQUM3Qjs7O0FBQ0E7SUFDSSxXQUFXO0FBQ2Y7OztBQUNBO0lBQ0ksc0JBQXNCO0FBQzFCOzs7QUFDQTtFQUNFLHlCQUF5QjtBQUMzQjs7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCOzs7QUFDQTtFQUNFLGFBQWE7QUFDZjs7O0FBQ0E7RUFDRSxjQUFjO0FBQ2hCOzs7QUFDQTtFQUNFLE9BQU87RUFHUCxnQ0FBZ0M7QUFDbEM7OztBQUNBOzs2REFFNkQ7OztBQUM3RDtJQUNJLGNBQWM7SUFDZCxxQ0FBcUM7SUFDckMsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsc0JBQXNCO0FBQzFCOzs7QUFDQTs7SUFFSSxlQUFlO0lBQ2YsZ0JBQWdCO0VBQ2xCOzs7QUFDRjs7SUFFSSxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOzs7QUFDQTs7SUFFSSxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOzs7QUFDQTs7SUFFSSxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOzs7QUFDQTs7SUFFSSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7QUFDbEI7OztBQUNBOztJQUVJLGVBQWU7QUFDbkI7OztBQUNBO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsdUJBQXVCO0FBQzNCOzs7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLGVBQWU7QUFDbkI7OztBQUNBO0VBR0UsZ0NBQWdDO0FBQ2xDOzs7QUFDQTtFQUdFLGdEQUFnRDtBQUNsRDs7O0FBQ0E7OzhEQUU4RDs7O0FBQzlEO0VBQ0Usb0JBQW9CO0FBQ3RCOzs7QUFDQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7OztBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7O0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMkJBQTJCO0VBQzNCLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0FBQ3pCOzs7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0FBQ2Q7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixtQkFBbUI7QUFDckI7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHlCQUF5QjtBQUMzQjs7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsb0JBQW9CO0FBQ3RCOzs7QUFDQTtFQUNFLDZCQUE2QjtFQUM3QixjQUFjO0VBQ2QseUJBQXlCO0FBQzNCOzs7QUFDQTtFQUNFLGNBQWM7QUFDaEI7OztBQUNBO0VBQ0UsV0FBVztBQUNiOzs7QUFDQTtFQUNFLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSxjQUFjO0FBQ2hCOzs7QUFDQSxrQkFBa0I7OztBQUNsQjtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2Isa0JBQWtCO0FBQ3BCOzs7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixTQUFTO0FBQ1g7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLGFBQWE7RUFDYixZQUFZOztFQUVaLDJEQUEyRDtFQUMzRCxzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixrQkFBa0I7RUFDbEIsWUFBWTtBQUNkOzs7QUFDQTtBQUNBLG9DQUFvQztBQUNwQyxxQkFBcUI7QUFDckIsMkRBQTJEO0FBQzNELDBCQUEwQjtBQUMxQiwyQkFBMkI7QUFDM0IsaUJBQWlCO0FBQ2pCOzs7QUFFQTtBQUNBO0FBQ0EsaUNBQXFEO0FBQ3JEO0FBQ0E7QUFDQSw2REFBNkQ7QUFDN0Q7QUFDQTtBQUNBLDZEQUE2RDtBQUM3RDtBQUNBO0FBQ0EsNkRBQTZEO0FBQzdEO0FBQ0E7QUFDQSw2REFBNkQ7QUFDN0Q7QUFDQTs7O0FBRUEsd0NBQXdDOzs7QUFDeEM7QUFDQSxVQUFVO0FBQ1YsaUJBQWlCO0FBQ2pCLEtBQUs7QUFDTCxRQUFRO0FBQ1IsTUFBTTtBQUNOLE9BQU87QUFDUCxtQ0FBbUM7QUFDbkMsV0FBVztBQUNYOzs7QUFLQTtFQUNFLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztBQUNYOzs7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQUNsQjs7O0FBQ0E7RUFDRSxnQkFBZ0I7QUFDbEI7OztBQUNBO0VBQ0U7O0FBRUY7OztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixrQkFBa0I7QUFDcEI7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLCtDQUFtRTtFQUNuRSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLDJCQUEyQjtFQUMzQiw4QkFBOEI7RUFDOUIsdUNBQXVDO0FBQ3pDOzs7QUFDQTtFQUNFLFVBQVU7RUFDVixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw0QkFBNEI7RUFDNUIsK0JBQStCO0FBQ2pDOzs7QUFDQTtFQUNFLHlCQUF5QjtFQUd6QixnQ0FBZ0M7QUFDbEM7OztBQUNBO0VBQ0UsY0FBYztBQUNoQjs7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGFBQWE7RUFDYixnQkFBZ0I7QUFDbEI7OztBQUNBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsU0FBUzs7QUFFWDs7O0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0FBQ2hCOzs7QUFDQTs7OERBRThEOzs7QUFDOUQ7RUFDRSxXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLDJCQUEyQjtBQUM3Qjs7O0FBQ0E7Q0FDQyx5QkFBeUI7QUFDMUI7OztBQUNBO0NBQ0MsZUFBZTtBQUNoQjs7O0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsVUFBVTtFQUNWLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsaUJBQWlCO0VBQ2pCLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxXQUFXO0VBQ1gsWUFBWTtBQUNkOzs7QUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7OztBQUNBOzs4REFFOEQ7OztBQUM5RDtFQUNFLHlCQUF5QjtFQUN6Qix5QkFBeUI7QUFDM0I7OztBQUNBO0VBQ0UsbUJBQW1CO0FBQ3JCOzs7QUFDQTtFQUdFLCtDQUErQztFQUMvQyxXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFFZixzQ0FBc0M7QUFDeEM7OztBQUNBO0VBQ0UsbURBQW1EO0lBRWpELG9DQUFvQztBQUN4Qzs7O0FBQ0E7RUFDRSxXQUFXO0FBQ2I7OztBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQix5QkFBeUI7QUFDM0I7OztBQUNBO0VBQ0UsV0FBVztFQUNYLFdBQVc7QUFDYjs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixpQkFBaUI7QUFDbkI7OztBQUNBO0VBQ0UsV0FBVztFQUNYLFdBQVc7QUFDYjs7O0FBQ0E7RUFDRSxVQUFVO0VBQ1YsWUFBWTtBQUNkOzs7QUFDQTtFQUNFLGVBQWU7RUFDZixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOzs7QUFDQTtFQUNFLFVBQVU7RUFDVixZQUFZO0FBQ2Q7OztBQUNBO0VBQ0UsMEJBQTBCO0VBQzFCLGVBQWU7RUFDZixjQUFjO0FBQ2hCOzs7QUFDQTtFQUNFLFVBQVU7RUFDVjtBQUNGOzs7QUFDQTtFQUNFLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsY0FBYztBQUNoQjs7O0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7OztBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixzQkFBc0I7QUFDeEI7OztBQUNBOzs4REFFOEQ7OztBQUM5RDtFQUNFLHNCQUFzQjtFQUN0Qix5QkFBeUI7QUFDM0I7OztBQUNBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7OztBQUNBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFHaEIsK0NBQStDO0FBQ2pEOzs7QUFDQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsa0JBQWtCO0FBQ3BCOzs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOzs7QUFDQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsaUJBQWlCO0FBQ25COzs7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCOzs7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjs7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLDBCQUEwQjtBQUM1Qjs7O0FBQ0E7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEI7OztBQUNBO0VBQ0UsV0FBVztFQUNYLFdBQVc7RUFDWCwwQkFBMEI7QUFDNUI7OztBQUNBO0VBQ0UsY0FBYztFQUNkLGVBQWU7RUFDZix1Q0FBK0I7VUFBL0IsK0JBQStCO0VBQy9CLGdCQUFnQjtBQUNsQjs7O0FBQ0E7RUFDRSx5QkFBeUI7QUFDM0I7OztBQUNBO0VBQ0UsY0FBYztBQUNoQjs7O0FBQ0E7RUFDRSxjQUFjO0FBQ2hCOzs7QUFDQSxpQkFBaUI7OztBQUNqQjtFQUNFLDBCQUEwQjtBQUM1Qjs7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCOzs7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsYUFBYTtFQUNiLGNBQWM7RUFDZCxxQ0FBeUQ7RUFDekQsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLFlBQVk7QUFDZDs7O0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsZUFBZTtBQUNqQjs7O0FBQ0E7OzhEQUU4RDs7O0FBQzlEO0VBQ0UseUJBQXlCO0VBQ3pCLG9CQUFvQjtBQUN0Qjs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtBQUNyQjs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjs7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztBQUNYOzs7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjs7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7OztBQUNBO0VBQ0UsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixrQkFBa0I7QUFDcEI7OztBQUNBO0VBQ0UsMEJBQTBCO0FBQzVCOzs7QUFDQTs7OERBRThEOzs7QUFDOUQ7RUFDRSxpQkFBaUI7RUFDakIsc0JBQXNCO0FBQ3hCOzs7QUFDQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQjs7O0FBQ0E7RUFDRSxzREFBc0Q7QUFDeEQ7OztBQUNBO0VBQ0UsMERBQTBEO0FBQzVEOzs7QUFDQTtFQUNFLDBEQUEwRDtBQUM1RDs7O0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLG1CQUFtQjtBQUNyQjs7O0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsVUFBVTtFQUNWLGVBQWU7RUFDZixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGFBQWE7QUFDZjs7O0FBQ0E7RUFDRSwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixXQUFXO0FBQ2I7OztBQUNBO0VBQ0UsaUJBQWlCO0FBQ25COzs7QUFDQTtFQUNFLFdBQVc7QUFDYjs7O0FBQ0E7RUFDRSxVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixXQUFXO0FBQ2I7OztBQUNBO0VBQ0Usb0JBQW9CO0FBQ3RCOzs7QUFDQTs7OERBRThEOzs7QUFDOUQ7RUFDRSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7OztBQUNBO0VBQ0UsZ0NBQWdDO0VBQ2hDLG9CQUFvQjtBQUN0Qjs7O0FBQ0E7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsZ0JBQWdCO0FBQ2xCOzs7QUFDQTtFQUNFLHlCQUF5QjtFQUN6QixVQUFVO0VBQ1YsWUFBWTtFQUNaLFdBQVc7RUFDWCx3Q0FBNEQ7RUFDNUQsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixnQkFBZ0I7QUFDbEI7OztBQUNBO0VBQ0UseUJBQXlCO0VBR3pCLGdDQUFnQztBQUNsQzs7O0FBQ0E7RUFDRSxrQkFBa0I7QUFDcEI7OztBQUNBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjs7O0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7OztBQUNBO0NBQ0MsWUFBWTtDQUNaLGVBQWU7QUFDaEI7OztBQUNBO0VBQ0Usb0JBQW9CO0FBQ3RCOzs7QUFDQTtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2IsZUFBZTtFQUNmLG9CQUFvQjtBQUN0Qjs7O0FBQ0E7OzhEQUU4RDs7O0FBQzlELG1EQUFtRDs7O0FBQ25EO0VBQ0U7SUFDRSxzQkFBc0I7SUFDdEIsV0FBVztJQUNYLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsWUFBWTtJQUdaLGdDQUFnQztFQUNsQztFQUNBO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGFBQWE7SUFDYixzQkFBc0I7RUFDeEI7RUFDQTtJQUNFLGVBQWU7SUFDZixjQUFjO0VBQ2hCO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsbUJBQW1CO0VBQ3JCO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLFdBQVc7SUFDWCxrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLFdBQVc7SUFDWCxZQUFZO0lBQ1osY0FBYztJQUNkLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0UsV0FBVztJQUNYLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIsY0FBYztJQUNkLGVBQWU7RUFDakI7RUFDQTtJQUNFLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixvQkFBb0I7RUFDdEI7RUFDQTtJQUNFLFdBQVc7SUFDWCxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxXQUFXO0VBQ2I7RUFDQTtJQUNFLFlBQVk7RUFDZDtFQUNBO0lBQ0UsVUFBVTtFQUNaO0VBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7QUFDRjs7O0FBQ0E7RUFDRTtJQUNFLFdBQVc7RUFDYjtFQUNBO0lBQ0UsZ0JBQWdCO0VBQ2xCO0VBQ0E7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixpQkFBaUI7RUFDbkI7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixTQUFTO0lBQ1QsbUJBQW1CO0VBQ3JCO0VBQ0E7SUFDRSxXQUFXO0VBQ2I7RUFDQTtJQUNFLFlBQVk7RUFDZDtBQUNGOzs7QUFDQTtFQUNFO0lBQ0UsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsV0FBVztFQUNiO0VBQ0E7SUFDRSxTQUFTO0VBQ1g7RUFDQTtJQUNFLFVBQVU7SUFDVixnQkFBZ0I7RUFDbEI7QUFDRjs7O0FBQ0E7RUFDRTtJQUNFLGtCQUFrQjtFQUNwQjtFQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLGdCQUFnQjtFQUNsQjtFQUNBO0lBQ0Usa0JBQWtCO0VBQ3BCO0FBQ0Y7OztBQUNBO0VBQ0U7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxVQUFVO0VBQ1o7RUFDQTtJQUNFLFlBQVk7RUFDZDtFQUNBO0lBQ0UsVUFBVTtJQUNWLGNBQWM7SUFDZCxnQkFBZ0I7RUFDbEI7RUFDQTtJQUNFLFVBQVU7SUFDVixlQUFlO0lBQ2YsZ0JBQWdCO0VBQ2xCO0FBQ0Y7OztBQUNBO0VBQ0U7SUFDRSxlQUFlO0VBQ2pCO0VBQ0E7SUFDRSxXQUFXO0lBQ1gsVUFBVTtFQUNaO0VBQ0E7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjtFQUNBO0lBQ0UsWUFBWTtFQUNkO0VBQ0E7SUFDRSxVQUFVO0VBQ1o7QUFDRjs7O0FBQ0E7RUFDRTtJQUNFLFNBQVM7RUFDWDtFQUNBO0lBQ0UsWUFBWTtFQUNkO0VBQ0E7SUFDRSxVQUFVO0VBQ1o7RUFDQTtJQUNFLFVBQVU7RUFDWjtFQUNBO0lBQ0UsVUFBVTtJQUNWLGVBQWU7SUFDZixrQkFBa0I7RUFDcEI7RUFDQTtJQUNFLGFBQWE7RUFDZjtBQUNGOzs7QUFHQTs7OERBRThEOzs7QUFDOUQ7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0FBQ3BCIiwiZmlsZSI6ImhvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIFRoZW1lIE5hbWU6IEx1eGVzdGF0ZSBIVE1MIFRlbXBsYXRlXHJcbiAgICBUaGVtZSBVUkk6IFxyXG4gICAgRGVzY3JpcHRpb246IEhUTUwgVGVtcGxhdGVcclxuICAgIEF1dGhvcjogUm9rdmljIFNhc2EgLyBHcmFwaGJlcnJ5XHJcbiAgICBBdXRob3IgVVJJOiBodHRwczovL3d3dy5ncmFwaGJlcnJ5LmNvbS9cclxuICAgIFZlcnNpb246IDEuMFxyXG4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuXHJcblxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAxICBHZW5lcmFsIFN0eWxlc1xyXG4gICAgMiAgVHlwb2dyYXBoeVxyXG4gICAgMyAgSGVhZGVyXHJcbiAgICA0ICBTdGF0aXN0aWNcclxuICAgIDUgIEFwcGFydG1lbnRzICBcclxuICAgIDYgIEhvdyBpdCB3b3Jrc1xyXG4gICAgNyAgQWdlbnRzXHJcbiAgICA4ICBBZGRzXHJcbiAgICA5ICBGb290ZXJcclxuICAgIDEwIFJlc3BvbnNpdmUgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgMS4gR2VuZXJhbCBTdHlsZXMgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4qIHtcclxuICAgIC8qIG1hcmdpbjogMCBhdXRvOyAqL1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5odG1sIHtcclxuICAgIC13ZWJraXQtYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgfVxyXG4qLCAqOmJlZm9yZSwgKjphZnRlciB7XHJcbiAgICAtd2Via2l0LWJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgICAtbW96LWJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgICBib3gtc2l6aW5nOiBpbmhlcml0O1xyXG59XHJcbmEge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgY29sb3I6ICMxRjM3M0Q7XHJcbn1cclxuYm9keSB7XHJcbiAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcclxuICAgIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7XHJcbiAgICB0ZXh0LXJlbmRlcmluZzogb3B0aW1pemVMZWdpYmlsaXR5O1xyXG4gICAgdGV4dC1zaGFkb3c6IHJnYmEoMCwwLDAsLjAxKSAwIDAgMXB4O1xyXG59XHJcbmE6Zm9jdXMsXHJcbmE6aG92ZXIge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgY29sb3I6ICNmZmQwMDA7XHJcbn1cclxuaW1nIHtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogYXV0bztcclxufVxyXG46OnNlbGVjdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMUYzNzNEO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG46Oi1tb3otc2VsZWN0aW9uIHtcclxuICAgIGJhY2tncm91bmQ6ICMxRjM3M0Q7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRleHQtc2hhZG93OiBub25lO1xyXG59XHJcbjo6LXdlYmtpdC1zZWxlY3Rpb24ge1xyXG4gICAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdGV4dC1zaGFkb3c6IG5vbmU7XHJcbn1cclxuOmFjdGl2ZSxcclxuOmZvY3VzIHtcclxuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG4udGV4dC1sZWZ0IHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtcmlnaHQge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcbi5sZWZ0IHtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG4ucmlnaHQge1xyXG4gIGZsb2F0OiByaWdodDtcclxufVxyXG4uY2VudGVyIHtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG4uZmxleC1jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4udXBwZXJjYXNlIHtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuLndoaXRlLXRleHQge1xyXG4gICAgY29sb3I6ICNGRkY7XHJcbn1cclxuLmxpZ2h0LWJnIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLnllbGxvdy1iZyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZkYWQwMDtcclxufVxyXG4ucG9pbnRlciB7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5oaWRlIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5zaG93IHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4uc2xpZGUge1xyXG4gIGxlZnQ6IDA7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC40cyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG59XHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDIuIFR5cG9ncmFwaHkgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmJvZHkge1xyXG4gICAgY29sb3I6ICMxRjM3M0Q7XHJcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxM3B0O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuICAgIHRyYW5zaXRpb246IG9wYWNpdHkgMXM7XHJcbn1cclxuaDEsXHJcbi5oMSB7XHJcbiAgICBmb250LXNpemU6IDU4cHg7XHJcbiAgICBmb250LXdlaWdodDogNzAwOyAgXHJcbiAgfVxyXG5oMixcclxuLmgyIHtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7ICBcclxufVxyXG5oMyxcclxuLmgzIHtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7ICBcclxufVxyXG5oNCxcclxuLmg0IHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7ICBcclxufVxyXG5oNSxcclxuLmg1IHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjb2xvcjogIzkxOUVCMTtcclxufVxyXG5oNixcclxuLmg2IHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5wIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMDI1ZW07XHJcbn1cclxubGkge1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAwLjAyNWVtO1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG59XHJcbi5hbmltYXRlIHtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbn1cclxuLmx1eC1zaGFkb3cge1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggMTVweCAwcHggcmdiYSgyOSwzMSwzNiwwLjEpO1xyXG4gIC1tb3otYm94LXNoYWRvdzogMHB4IDEwcHggMTVweCAwcHggcmdiYSgyOSwzMSwzNiwwLjEpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAxMHB4IDE1cHggMHB4IHJnYmEoMjksMzEsMzYsMC4xKTtcclxufVxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAzLiBIZWFkZXJcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmhlYWRlciB7XHJcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbn1cclxuaGVhZGVyIG5hdiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICB6LWluZGV4OiA5OTk5O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLnNtYWxsLW5hdiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4ubG9nbyB7XHJcbiAgLyogd2lkdGg6IDIwJTsgKi9cclxuICAvKiBoZWlnaHQ6IDk1cHg7ICovXHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICBmb250LXNpemU6IDE4cHQ7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG4ubWVudSB7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbn1cclxuLnBhZ2UtbWVudSB7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi5yZWdpc3RyYXRpb24ge1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG59XHJcbi5qb2luLXVzIHtcclxuICB3aWR0aDogNDAlO1xyXG4gIGhlaWdodDogOTVweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG59XHJcbi5nZXR0aW5nLXN0YXJ0ZWQge1xyXG4gIHdpZHRoOiA2MCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG59XHJcbi5tYWluLWJ0biB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDAwMDtcclxuICBwYWRkaW5nOiAxMHB4IDEwcHg7XHJcbiAgbWFyZ2luOiAwcHggNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDAwMDtcclxuICB0cmFuc2l0aW9uOiAwLjRzIGFsbDtcclxufVxyXG4ubWFpbi1idG46aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmQwMDA7XHJcbn1cclxuLm1haW4tYnRuOmhvdmVyIGEge1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG59XHJcbi5tYWluLWJ0biBhIHtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG4ucGFnZS1tZW51IGxpIHtcclxuICBwYWRkaW5nOiA4cHg7XHJcbn1cclxuLmFjdGl2ZSB7XHJcbiAgY29sb3I6ICNmZmQwMDA7XHJcbn1cclxuLyotLSBIZXJvIEFyZWEgLS0qL1xyXG4uaGVybyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA3MDBweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLnRpdGxlIHtcclxuICB3aWR0aDogNjAlO1xyXG4gIGhlaWdodDogMzB2aDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogOTk5O1xyXG4gIHRvcDogMjN2aDtcclxufVxyXG4uaGVyby1pbWFnZSB7XHJcbiAgd2lkdGg6IDg4JTtcclxuICBoZWlnaHQ6IDYzMHB4O1xyXG4gIGZsb2F0OiByaWdodDtcclxuXHJcbiAgLyogYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2hlcm8ucG5nJyk7ICovXHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcmlnaHQ6IC02MHB4O1xyXG59XHJcbiNhbmltYXRlZC1iYWNrZ3JvdW5ke1xyXG4vKiBhbmltYXRpb246IGJnZmFkZSAxNXMgaW5maW5pdGU7ICovXHJcbmJhY2tncm91bmQtc2l6ZTpjb3ZlcjtcclxuLyogYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2hlcm8ucG5nJyk7ICovXHJcbmJhY2tncm91bmQtcG9zaXRpb246Y2VudGVyO1xyXG4vKiBiYWNrZ3JvdW5kLWNvbG9yOiMwMDA7ICovXHJcbnBvc2l0aW9uOnJlbGF0aXZlO1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJnZmFkZSB7XHJcbjAlIHtcclxuYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2hlcm8ucG5nJyk7XHJcbn1cclxuMjUlIHtcclxuLyogYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2hlcm8gMS5qcGcnKTsgKi9cclxufVxyXG41MCUge1xyXG4vKiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvaGVybyAyLmpwZycpOyAqL1xyXG59XHJcbjc1JSB7XHJcbi8qIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltYWdlcy9oZXJvIDMuanBnJyk7ICovXHJcbn1cclxuMTAwJSB7XHJcbi8qIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltYWdlcy9oZXJvIDQuanBnJyk7ICovXHJcbn1cclxufVxyXG5cclxuLyoqKioqKiBhZGQgYSBiYWNrZ3JvdW5kIG92ZXJsYXkgKioqKioqKi9cclxuI2FuaW1hdGVkLWJhY2tncm91bmQ6YmVmb3JlIHtcclxuY29udGVudDpcIlwiO1xyXG5wb3NpdGlvbjphYnNvbHV0ZTtcclxudG9wOjA7XHJcbmJvdHRvbTowO1xyXG5sZWZ0OjA7XHJcbnJpZ2h0OjA7XHJcbmJhY2tncm91bmQtY29sb3I6cmdiKDI1NSwgMjU1LCAyNTUpO1xyXG5vcGFjaXR5OjAuMjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGJvdHRvbTogMDtcclxufVxyXG4uaGVyby1pbWFnZS1pbmZvIHAge1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMjBweDtcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG59XHJcbi5pbmZvIHtcclxuICBtYXJnaW46IDVweCAzNXB4O1xyXG59XHJcbi5pbmZvIHB7XHJcbiAgY29sb3I6ICNmZmZcclxuXHJcbn1cclxuLnNlYXJjaCB7XHJcbiAgbWFyZ2luLXRvcDogNDBweDtcclxuICB3aWR0aDogNTAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5zZWFyY2ggaW5wdXQge1xyXG4gIHdpZHRoOiA3NSU7XHJcbiAgYmFja2dyb3VuZDogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL3NlYXJjaC5zdmcnKSBuby1yZXBlYXQgOHB4IDhweDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0O1xyXG4gIGJvcmRlcjogMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogMjVweCA0MHB4O1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XHJcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNXB4O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMTVweCB0b3AgMjlweDtcclxufVxyXG4uc2VhcmNoLWJ0biB7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBib3JkZXI6IDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHBhZGRpbmc6IDI1cHggMTBweDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA1cHg7XHJcbn1cclxuLnNlYXJjaC1idG46aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmQwMDA7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG59XHJcbjo6cGxhY2Vob2xkZXIge1xyXG4gIGNvbG9yOiAjOTE5RUIxO1xyXG59XHJcbi5oZXJvLXdyYXBwZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcyNXB4O1xyXG4gIHBhZGRpbmctdG9wOjk1cHg7XHJcbn1cclxuLnNsaWRlLWRvd24ge1xyXG4gIHdpZHRoOjIwcHg7XHJcbiAgaGVpZ2h0OiA2NXB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDA7XHJcblxyXG59XHJcbi5zbGlkZS1kb3duIHAge1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgY29sb3I6ICM5MTlFQjE7XHJcbn1cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgNC4gU1RBVElTVElDXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4uc3RhdGlzdGljIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBtaW4taGVpZ2h0OiA2NTBweDtcclxuICBwYWRkaW5nOiAxNDVweCAwcHggODBweCAwcHg7XHJcbn1cclxuLnN0YXRpc3RpYyBoMyB7XHJcbiBwYWRkaW5nOjE1cHggMHB4IDUwcHggMHB4O1xyXG59XHJcbi5zdGF0aXN0aWMgcCB7XHJcbiBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuLnN0YXRpc3RpYy1ib3gge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNGNkY1RjQ7XHJcbiAgd2lkdGg6IDM2JTtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIG1hcmdpbjogMHB4IDBweCAzMHB4IDMwcHg7XHJcbiAgcGFkZGluZy10b3A6IDQwcHg7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5zdGF0aXN0aWMtYm94IHA6bnRoLWNoaWxkKDEpIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogI2ZmZDAwMDtcclxuICBtYXJnaW46IDBweDtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuLnN0YXRpc3RpYy1ib3ggcDpudGgtY2hpbGQoMikge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICA1LiBBUFBBUlRNRU5UU1xyXG4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuLmFwcGFydG1lbnRzIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjZGNUY0O1xyXG4gIHBhZGRpbmc6NjBweCAwcHggNzBweCAwcHg7XHJcbn1cclxuLmFwcGFydG1lbnRzIGgyIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcbi5hcHBhcnRtZW50LWJveCB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDhweCAwcHggcmdiYSgyNCw0OSw1NiwwLjE1KTtcclxuICAtbW96LWJveC1zaGFkb3c6IDBweCAycHggOHB4IDBweCByZ2JhKDI0LDQ5LDU2LDAuMTUpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAycHggOHB4IDBweCByZ2JhKDI0LDQ5LDU2LDAuMTUpO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNTAwcHg7XHJcbiAgbWF4LXdpZHRoOiAzNTBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBtYXJnaW4tdG9wOiAzNXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICAtd2Via2l0LXRyYW5zaXRpb246ICBib3gtc2hhZG93IDIwMG1zIGVhc2Utb3V0O1xyXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMjAwbXMgZWFzZS1vdXQgO1xyXG59XHJcbi5hcHBhcnRtZW50LWJveDpob3ZlcntcclxuICBib3gtc2hhZG93OiAxcHggNHB4IDEwcHggcmdiYSgxMzYsIDEzNiwgMTM2LCAwLjgzNik7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246ICBib3gtc2hhZG93IDIwMG1zIGVhc2UtaW47XHJcbiAgICB0cmFuc2l0aW9uOiBib3gtc2hhZG93IDIwMG1zIGVhc2UtaW47XHJcbn1cclxuLmFwcGFydG1lbnQtaW1hZ2Uge1xyXG4gIGhlaWdodDogNzYlO1xyXG59XHJcbi5hcHBhcnRtZW50LWltYWdlIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5hcHBhcnRtZW50LWluZm8ge1xyXG4gIGhlaWdodDogMjUlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogMjVweCAyMHB4O1xyXG4gIC8qIHBhZGRpbmctYm90dG9tOiA1MHB4ICovXHJcbn1cclxuLmFwcGFydG1lbnQtdGl0bGUge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNTAlO1xyXG59XHJcbi5hcHBhcnRtZW50LXRpdGxlIHAge1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbn1cclxuLmFwcGFydG1lbnQtZGV0YWlscyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA1MCU7XHJcbn1cclxuLnByaWNlIHtcclxuICB3aWR0aDogNTAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4ucHJpY2UgcCB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGNvbG9yOiAjZmZkMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuLmJhdGhyb29tcyB7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLmJhdGhyb29tcyBwIHtcclxuICBwYWRkaW5nOiAxNXB4IDBweCAwcHggMTBweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgY29sb3I6ICM5MTlFQjE7XHJcbn1cclxuLmJlZHJvb21zIHtcclxuICB3aWR0aDogMjUlO1xyXG4gIGhlaWdodDogMTAwJVxyXG59XHJcbi5iZWRyb29tcyBwIHtcclxuICBwYWRkaW5nOiAxNXB4IDBweCAwcHggMTBweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgY29sb3I6ICM5MTlFQjE7XHJcbn1cclxuLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgcG9zaXRpb246IGluaGVyaXQ7XHJcbn1cclxuLnNlYXJjaC1hbGwtYnRuIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxufVxyXG4vKi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICA2LiBIT1cgSVQgV09SS1NcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbi5ob3ctaXQtd29ya3Mge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzo2MHB4IDBweCAyMHB4IDBweDtcclxufVxyXG4ud29ya3MtdGl0bGUgcCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDEuOXJlbTtcclxufVxyXG4ud29yay1ib3gge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMzcycHg7XHJcbiAgbWF4LXdpZHRoOiAzNDVweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBtYXJnaW4tdG9wOiA2MHB4O1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDhweCAwcHggcmdiYSgyNCw0OSw1NiwwLjE1KTtcclxuICAtbW96LWJveC1zaGFkb3c6IDBweCAycHggOHB4IDBweCByZ2JhKDI0LDQ5LDU2LDAuMTUpO1xyXG4gIGJveC1zaGFkb3c6IDBweCAycHggOHB4IDBweCByZ2JhKDI0LDQ5LDU2LDAuMTUpO1xyXG59XHJcbi53b3JrLWJveC1udW1iZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMzUlO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4ud29yay1ib3gtbnVtYmVyIHAge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IC0xMjRweDtcclxuICBsZWZ0OiAtNDVweDtcclxuICBmb250LXNpemU6IDE2MHB4O1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuLndvcmstYm94LXRpdGxlIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDE1JTtcclxuICBwYWRkaW5nOiAwcHggMjBweDtcclxufVxyXG4ud29yay1ib3gtdGl0bGUgcCB7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogIzFGMzczRDtcclxufVxyXG4ud29yay1ib3gtdGl0bGUgcDpudGgtY2hpbGQoMSkge1xyXG4gIG1hcmdpbi1yaWdodDogMTVweDtcclxufVxyXG4ud29yay1ib3gtdGV4dCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAzMCU7XHJcbiAgcGFkZGluZzogMHB4IDIwcHggMHB4IDU1cHg7XHJcbn1cclxuLndvcmstYm94LXRleHQgcCB7XHJcbiAgY29sb3I6ICMxRjM3M0Q7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGxpbmUtaGVpZ2h0OiAycmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuLndvcmstYm94LWxpbmsge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMjAlO1xyXG4gIHBhZGRpbmc6IDBweCAxMHB4IDBweCA1NXB4O1xyXG59XHJcbi53b3JrLWJveC1saW5rIGEge1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICB0ZXh0LWRlY29yYXRpb24tbGluZTogdW5kZXJsaW5lO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuLndvcmstYm94OmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG59XHJcbi53b3JrLWJveDpob3ZlciBwe1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi53b3JrLWJveDpob3ZlciBhe1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi8qIFdvcmsgU2VydmljZSAqL1xyXG4ud29yay1zZXJ2aWNlcyB7XHJcbiAgcGFkZGluZzogOTBweCAwcHggNjVweCAwcHg7XHJcbn1cclxuLndvcmstc2VydmljZXMtdGl0bGUgcCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcbi53b3JrLXNlcnZpY2UtaW1hZ2Uge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGFxdWE7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA2MzBweDtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2VydmljZXMucG5nJyk7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogLTEwNXB4O1xyXG59XHJcbi53b3JrLXNlcnZpY2UtaW5mbyBwIHtcclxuICBwYWRkaW5nOiAxNXB4IDBweCA1MHB4IDBweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgNy4gQUdFTlRTXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4uYWdlbnRzIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjZGNUY0O1xyXG4gIHBhZGRpbmctYm90dG9tOiA5MHB4O1xyXG59XHJcbi5hZ2VudHMtdGl0bGUgcCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDEuOXJlbTtcclxufVxyXG4uYWdlbnQtbmFtZSBwIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgY29sb3I6ICNlOWJlMDE7XHJcbn1cclxuLmFnZW50LXNvY2lhbCBhIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7IFxyXG4gIHRvcDogLTVweDtcclxufVxyXG4uYWdlbnQtc29jaWFsIGEge1xyXG4gIC8qIHBhZGRpbmc6IDVweDsgKi9cclxufVxyXG4uYWdlbnQtc29jaWFsIGE6bnRoLWNoaWxkKDIpIHtcclxuICAvKiBtYXJnaW46IDBweCAxNXB4OyAqL1xyXG59XHJcbi5hcHBhcnRtZW50LWluZm8ge1xyXG4gIGhlaWdodDogMjQlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogMjVweCAyMHB4O1xyXG59XHJcbi5hZ2VudHMtc2VydmljZXMge1xyXG4gIHBhZGRpbmc6IDkwcHggMHB4IDM1cHggMHB4O1xyXG59XHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDguIEFERFNcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbi5hZGQge1xyXG4gIHBhZGRpbmc6IDkwcHggMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuLmFkZC1pbWFnZSB7XHJcbiAgd2lkdGg6IDU0MHB4O1xyXG4gIGhlaWdodDogMzMwcHg7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLWJvdHRvbTogODBweDtcclxufVxyXG4uYWRkMSB7XHJcbiAgLyogYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2FkZCcpOyAqL1xyXG59XHJcbi5hZGQyIHtcclxuICAvKiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvYWRkLnBuZycpOyAqL1xyXG59XHJcbi5hZGQzIHtcclxuICAvKiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvYWRkLnBuZycpOyAqL1xyXG59XHJcbi5hZGQtaW5mbyBwIHtcclxuICBwYWRkaW5nOiAxNXB4IDBweCA1MHB4IDBweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDEuOHJlbTtcclxufVxyXG4uYWRkLWltYWdlLWluZm8ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmQwMDA7XHJcbiAgd2lkdGg6IDYwJTtcclxuICBtYXJnaW46IDBweCAyMCU7XHJcbiAgaGVpZ2h0OiA4MHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IC0zMHB4O1xyXG59XHJcbi5hZGQtaW1hZ2UtaW5mbyBwIHtcclxuICBwYWRkaW5nOiAxNXB4IDBweCAwcHggMjBweDtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG4uYWRkMi1pbmZvIHtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uYWRkLWltYWdlLWluZm8yIHtcclxuICBsZWZ0OiAxNTBweDtcclxufVxyXG4uc2hvdy1tb3JlLWJ0biB7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG4uYWRkMy1pbmZvIHAge1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDkuIEZvb3RlclxyXG4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuZm9vdGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzMzMzMzO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHBhZGRpbmc6IDY1cHggMHB4O1xyXG59XHJcbi5mb290ZXItdG9wIHtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzkxOUVCMTtcclxuICBwYWRkaW5nLWJvdHRvbTogNDVweDtcclxufVxyXG4uTmV3c2xldHRlciBpbnB1dCB7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgcGFkZGluZzogMHB4IDE1cHg7XHJcbiAgYm9yZGVyOiAwcHg7XHJcbiAgbWFyZ2luLXRvcDogMjZweDtcclxufVxyXG4ubmV3c2xldHRlci1idG4ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmYmZiZmI7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgYm9yZGVyOiAwcHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1hZ2VzL2Fycm93LXJpZ2h0LnN2ZycpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDI2cHg7XHJcbn1cclxuLm5ld3NsZXR0ZXItYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxufVxyXG4uZm9vdGVyLWxvZ28gaDQge1xyXG4gIG1hcmdpbi1ib3R0b206NjBweDtcclxufVxyXG4uZm9vdGVyLWxvZ28gcCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuLmZvb3Rlci1ib3R0b20ge1xyXG4gIHBhZGRpbmctdG9wOiA3MHB4O1xyXG59XHJcbi5mb290ZXItYm90dG9tIGF7XHJcbiBjb2xvcjogd2hpdGU7XHJcbiBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuLmZvb3Rlci1jb2x1bW4gaDUge1xyXG4gIHBhZGRpbmctYm90dG9tOiAzNXB4O1xyXG59XHJcbi5mb290ZXItY29sdW1uIGEge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxOHB4O1xyXG59XHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDEwLiBSRVNQT05TSVZFXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4vKiBMYXJnZSBkZXZpY2VzIChsYXB0b3BzL2Rlc2t0b3BzLCA5OTJweCBhbmQgdXApICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAubWVudSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA3MHB4O1xyXG4gICAgcmlnaHQ6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC40cyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gIH1cclxuICAucGFnZS1tZW51IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MHZoO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZmxvYXQ6IG5vbmU7ICBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIH1cclxuICAucGFnZS1tZW51IGxpIHtcclxuICAgIGZvbnQtc2l6ZTogMTVwdDtcclxuICAgIHBhZGRpbmc6IDEuNXZoO1xyXG4gIH1cclxuICAucmVnaXN0cmF0aW9uIHtcclxuICAgIGZsb2F0OiBub25lO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDUwdmg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmctYm90dG9tOiA1dmg7XHJcbiAgfVxyXG4gIC5qb2luLXVzIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gIH1cclxuICAuam9pbi11cyBsaSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLm1lbnUtYnV0dG9uIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW4tdG9wOiAyNnB4O1xyXG4gIH1cclxuICAubWVudS1idXR0b24gc3BhbiB7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGhlaWdodDogNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzFGMzczRDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiA2cHggMHB4O1xyXG4gIH1cclxuICAubWFpbi1idG4ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gICAgcGFkZGluZzogMTBweCAxMHB4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZkMDAwO1xyXG4gICAgdHJhbnNpdGlvbjogMC40cyBhbGw7XHJcbiAgfVxyXG4gIC5zdGF0aXN0aWMtYm94Om50aC1jaGlsZCgxKSB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIG1hcmdpbi10b3A6NTBweDtcclxuICB9XHJcbiAgLnN0YXRpc3RpYy1ib3g6bnRoLWNoaWxkKDIpIHtcclxuICAgIG1hcmdpbi10b3A6NTBweDtcclxuICB9XHJcbiAgLnN0YXRpc3RpYy1ib3g6bnRoLWNoaWxkKDMpIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gIH1cclxuICAuc2VhcmNoLWFwcGFydG1lbnRzIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcbiAgLnNlYXJjaC1hbGwtYnRuIHtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgfVxyXG4gIC5ob3ctaXQtd29ya3Mge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxufVxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgLmhlcm8taW1hZ2Uge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIC5pbmZvIHtcclxuICAgIG1hcmdpbjogMHB4IDEwcHg7XHJcbiAgfVxyXG4gIGgxIHtcclxuICAgIGZvbnQtc2l6ZTogNDVweDtcclxuICB9XHJcbiAgLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgICB3aWR0aDogMjcwcHg7XHJcbiAgfVxyXG4gIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgfVxyXG4gIC53b3JrLXNlcnZpY2UtaW1hZ2Uge1xyXG4gICAgbWF4LWhlaWdodDogNjMwcHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG4gIH1cclxuICAuZm9vdGVyLWxvZ28gaDQge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG4gIC5mb290ZXItbG9nbyBwIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2NTBweCkge1xyXG4gIC5pbmZvIHAge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gIH1cclxuICAuaGVyby1pbWFnZS1pbmZvIHtcclxuICAgIHJpZ2h0OiA0MHB4O1xyXG4gIH1cclxuICAudGl0bGUge1xyXG4gICAgdG9wOiAxN3ZoO1xyXG4gIH1cclxuICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XHJcbiAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTc1cHgpIHtcclxuICAuZm9vdGVyLWJvdHRvbSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5mb290ZXItY29sdW1uIGEge1xyXG4gICAgZGlzcGxheTogbGlzdC1pdGVtO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICB9XHJcbiAgLmZvb3Rlci10b3B7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTQwcHgpIHtcclxuICAuaW5mbyBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcbiAgLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gIH1cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiA0MDBweDtcclxuICB9XHJcbiAgLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gIH1cclxuICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIG1hcmdpbjogMHB4IDEwJTtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDcwcHgpIHtcclxuICAuaW5mbyBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcbiAgLmhlcm8taW1hZ2UtaW5mbyB7XHJcbiAgICByaWdodDogMzBweDtcclxuICAgIHdpZHRoOiA5MSU7XHJcbiAgfVxyXG4gIC5oZXJvLWltYWdlLWluZm8gcCB7XHJcbiAgICBwYWRkaW5nOiA1cHggNXB4O1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gIH1cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcbiAgLnNlYXJjaC1hbGwtYnRuIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgfVxyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzgwcHgpIHtcclxuICAudGl0bGUge1xyXG4gICAgdG9wOiAyMnZoO1xyXG4gIH1cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiAyNTBweDtcclxuICB9XHJcbiAgLnNlYXJjaC1idG4ge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbiAgLnNlYXJjaC1hbGwtYnRuIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgfVxyXG4gIC5zZWFyY2ggaW5wdXQge1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHBhZGRpbmc6IDI3cHggNDBweDtcclxuICB9XHJcbiAgLmluZm8gaW1nIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgMTEuIGxvZ29cclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbi5sb2dve1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICAvKiBoZWlnaHQ6OTBweDsgICovXHJcbn0iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");



class AppComponent {
    constructor() {
        this.title = 'olyd';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], null, null); })();


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _all_properties_all_properties_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./all-properties/all-properties.component */ "jMce");
/* harmony import */ var _property_property_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./property/property.component */ "6Y1P");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "9vUh");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./about/about.component */ "84zG");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./footer/footer.component */ "fp1T");











class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _all_properties_all_properties_component__WEBPACK_IMPORTED_MODULE_4__["AllPropertiesComponent"],
        _property_property_component__WEBPACK_IMPORTED_MODULE_5__["PropertyComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
        _about_about_component__WEBPACK_IMPORTED_MODULE_8__["AboutComponent"],
        _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _all_properties_all_properties_component__WEBPACK_IMPORTED_MODULE_4__["AllPropertiesComponent"],
                    _property_property_component__WEBPACK_IMPORTED_MODULE_5__["PropertyComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                    _about_about_component__WEBPACK_IMPORTED_MODULE_8__["AboutComponent"],
                    _footer_footer_component__WEBPACK_IMPORTED_MODULE_9__["FooterComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "fp1T":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");



class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FooterComponent, selectors: [["app-footer"]], decls: 54, vars: 0, consts: [["id", "contact"], [1, "container"], [1, "row", "footer-top"], [1, "col-12", "col-md-6"], [1, "row", "footer-bottom"], [1, "col-sm-12", "col-md-3", "footer-logo"], ["href", "https://www.instagram.com/16th August 85, Villa_property_management_/", "target", "_blank"], ["src", "../../assets/images/instagram.svg", "alt", ""], ["href", "#", "target", "_blank"], ["src", "../../assets/images/envelope.svg", "alt", ""], ["href", "#"], ["src", "../../assets/images/phone-alt.svg", "alt", ""], [1, "col-sm-3", "col-md-1", "footer-column"], [1, "col-sm-3", "col-md-2", "footer-column", "right"], ["routerLink", "/about"], [1, "col-sm-3", "col-md-1", "footer-column", "right"], ["href", "#appartments"], ["href", "#works"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "16th August 85, Villa");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Property Management");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "16th August 85, Villa");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Contact us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "\u00A0 Instagram");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " \u00A0");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "\u00A0 Villaproperties@gmail.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "\u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "\u00A0 \u00A0 (+233) 322 031 764");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "\u00A9 2021 - 16th August 85, Villa,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "All Right Reserved");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "COMPANY");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "About");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "PRODUCT");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Appartments");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "How It Works");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb290ZXIuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-footer',
                templateUrl: './footer.component.html',
                styleUrls: ['./footer.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "jMce":
/*!************************************************************!*\
  !*** ./src/app/all-properties/all-properties.component.ts ***!
  \************************************************************/
/*! exports provided: AllPropertiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllPropertiesComponent", function() { return AllPropertiesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../footer/footer.component */ "fp1T");




const _c0 = function () { return ["/property", 0]; };
const _c1 = function () { return ["/property", 1]; };
const _c2 = function () { return ["/property", 2]; };
class AllPropertiesComponent {
    constructor() { }
    ngOnInit() {
    }
}
AllPropertiesComponent.ɵfac = function AllPropertiesComponent_Factory(t) { return new (t || AllPropertiesComponent)(); };
AllPropertiesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AllPropertiesComponent, selectors: [["app-all-properties"]], decls: 88, vars: 6, consts: [["id", "home"], [1, "animate"], [1, "container"], [1, "logo", "left"], ["routerLink", "/"], ["src", "../../assets/images/logo_.png", 1, "logo"], [1, "menu-button", "hide", "right", "pointer"], [1, "menu", "left"], [1, "page-menu", "left"], [1, "registration", "flex-center"], [1, "join-us"], [1, "pointer", "animate"], ["href", "#contact"], [1, "getting-started"], ["routerLink", "/", 1, "main-btn", "pointer", "text-center", "animate"], ["id", "appartments", 1, "appartments"], [1, "row"], [1, "col-12"], [1, "col-12", "col-md-6", "col-lg-4"], [1, "appartment-box", 3, "routerLink"], [1, "appartment-image"], ["src", "../../assets/images/apartments/temp.jpg", "alt", ""], [1, "appartment-info"], [1, "appartment-title"], [1, "appartment-details"], [1, "price", "left"], [1, "bedrooms", "right", "flex-center"], ["src", "../../assets/images/bed.svg", "alt", "", 1, "left"], [1, "left"], [1, "bathrooms", "right", "flex-center"], ["src", "../../assets/images/shower.svg", "alt", "", 1, "left"]], template: function AllPropertiesComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Properties available");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "p", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "img", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "p", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "5 BA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "Property name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Price");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "4 BD");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "img", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "4.5 BA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "app-footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c2));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_2__["FooterComponent"]], styles: ["*[_ngcontent-%COMP%] {\r\n  \r\n  padding: 0;\r\n}\r\n\r\nhtml[_ngcontent-%COMP%] {\r\n  box-sizing: border-box;\r\n}\r\n\r\n*[_ngcontent-%COMP%], *[_ngcontent-%COMP%]:before, *[_ngcontent-%COMP%]:after {\r\n  box-sizing: inherit;\r\n}\r\n\r\na[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  position: relative;\r\n  color: #1F373D;\r\n}\r\n\r\nbody[_ngcontent-%COMP%] {\r\n  -webkit-font-smoothing: antialiased;\r\n  -moz-osx-font-smoothing: grayscale;\r\n  text-rendering: optimizeLegibility;\r\n  text-shadow: rgba(0, 0, 0, .01) 0 0 1px;\r\n}\r\n\r\na[_ngcontent-%COMP%]:focus, a[_ngcontent-%COMP%]:hover {\r\n  text-decoration: none;\r\n  color: #ffd000;\r\n}\r\n\r\nimg[_ngcontent-%COMP%] {\r\n  max-width: 100%;\r\n  height: auto;\r\n}\r\n\r\n[_ngcontent-%COMP%]::selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n\r\n[_ngcontent-%COMP%]::-moz-selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n\r\n[_ngcontent-%COMP%]::-webkit-selection {\r\n  background: #1F373D;\r\n  color: #fff;\r\n  text-shadow: none;\r\n}\r\n\r\n[_ngcontent-%COMP%]:active, [_ngcontent-%COMP%]:focus {\r\n  outline: none !important;\r\n}\r\n\r\n.text-left[_ngcontent-%COMP%] {\r\n  text-align: left !important;\r\n}\r\n\r\n.text-right[_ngcontent-%COMP%] {\r\n  text-align: right !important;\r\n}\r\n\r\n.text-center[_ngcontent-%COMP%] {\r\n  text-align: center !important;\r\n}\r\n\r\n.left[_ngcontent-%COMP%] {\r\n  float: left;\r\n}\r\n\r\n.right[_ngcontent-%COMP%] {\r\n  float: right;\r\n}\r\n\r\n.center[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n}\r\n\r\n.flex-center[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.uppercase[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n}\r\n\r\n.white-text[_ngcontent-%COMP%] {\r\n  color: #FFF;\r\n}\r\n\r\n.light-bg[_ngcontent-%COMP%] {\r\n  background-color: #fff;\r\n}\r\n\r\n.yellow-bg[_ngcontent-%COMP%] {\r\n  background-color: #ffd000;\r\n}\r\n\r\n.pointer[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n}\r\n\r\n.hide[_ngcontent-%COMP%] {\r\n  display: none;\r\n}\r\n\r\n.show[_ngcontent-%COMP%] {\r\n  display: block;\r\n}\r\n\r\n.slide[_ngcontent-%COMP%] {\r\n  left: 0;\r\n  transition: all 0.4s ease-in-out;\r\n}\r\n\r\n\r\n\r\nbody[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n  font-family: 'Montserrat', sans-serif;\r\n  font-size: 13pt;\r\n  font-weight: 400;\r\n  overflow-x: hidden;\r\n  transition: opacity 1s;\r\n}\r\n\r\nh1[_ngcontent-%COMP%], .h1[_ngcontent-%COMP%] {\r\n  font-size: 58px;\r\n  font-weight: 700;\r\n}\r\n\r\nh2[_ngcontent-%COMP%], .h2[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n}\r\n\r\nh3[_ngcontent-%COMP%], .h3[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n}\r\n\r\nh4[_ngcontent-%COMP%], .h4[_ngcontent-%COMP%] {\r\n  font-size: 25px;\r\n  font-weight: 700;\r\n}\r\n\r\nh5[_ngcontent-%COMP%], .h5[_ngcontent-%COMP%] {\r\n  font-size: 14px;\r\n  font-weight: 400;\r\n  color: #919EB1;\r\n}\r\n\r\nh6[_ngcontent-%COMP%], .h6[_ngcontent-%COMP%] {\r\n  font-size: 14px;\r\n}\r\n\r\np[_ngcontent-%COMP%] {\r\n  font-size: 16px;\r\n  line-height: 1.8;\r\n  margin-bottom: 15px;\r\n  letter-spacing: 0.025em;\r\n}\r\n\r\nli[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  font-weight: 400;\r\n  letter-spacing: 0.025em;\r\n  display: inline;\r\n}\r\n\r\n.animate[_ngcontent-%COMP%] {\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n.lux-shadow[_ngcontent-%COMP%] {\r\n  box-shadow: 0px 10px 15px 0px rgba(29, 31, 36, 0.1);\r\n}\r\n\r\nheader[_ngcontent-%COMP%] {\r\n  padding-bottom: 25px;\r\n}\r\n\r\nheader[_ngcontent-%COMP%]   nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 95px;\r\n  position: fixed;\r\n  z-index: 9999;\r\n  background-color: #fff;\r\n}\r\n\r\n.animate[_ngcontent-%COMP%] {\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n.small-nav[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 70px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.logo[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-start;\r\n  font-size: 18pt;\r\n  font-weight: 700;\r\n  width: 120px;\r\n  \r\n}\r\n\r\n.menu[_ngcontent-%COMP%] {\r\n  width: 80%;\r\n  height: 95px;\r\n}\r\n\r\n.page-menu[_ngcontent-%COMP%] {\r\n  width: 70%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: center;\r\n}\r\n\r\n.registration[_ngcontent-%COMP%] {\r\n  width: 30%;\r\n  height: 95px;\r\n}\r\n\r\n.join-us[_ngcontent-%COMP%] {\r\n  width: 40%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n  padding-right: 20px;\r\n}\r\n\r\n.getting-started[_ngcontent-%COMP%] {\r\n  width: 60%;\r\n  height: 95px;\r\n  display: flex;\r\n  align-items: center;\r\n  justify-content: flex-end;\r\n}\r\n\r\n.main-btn[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  background-color: #ffd000;\r\n  padding: 10px 10px;\r\n  margin: 0px 5px;\r\n  border-radius: 5px;\r\n  font-size: 13px;\r\n  border: 1px solid #ffd000;\r\n  transition: 0.4s all;\r\n}\r\n\r\n.main-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: transparent;\r\n  color: #000000;\r\n  border: 1px solid #ffd000;\r\n}\r\n\r\n.main-btn[_ngcontent-%COMP%]:hover   a[_ngcontent-%COMP%] {\r\n  color: #1F373D;\r\n}\r\n\r\n.main-btn[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #fff;\r\n}\r\n\r\n.page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n  padding: 8px;\r\n}\r\n\r\n.active[_ngcontent-%COMP%] {\r\n  color: #ffd000;\r\n}\r\n\r\n.appartments[_ngcontent-%COMP%] {\r\n  background-color: #F6F5F4;\r\n  padding: 60px 0px 70px 0px;\r\n}\r\n\r\n.appartments[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\r\n  margin-bottom: 35px;\r\n}\r\n\r\n.appartment-box[_ngcontent-%COMP%] {\r\n  box-shadow: 0px 2px 8px 0px rgba(24, 49, 56, 0.15);\r\n  width: 100%;\r\n  height: 500px;\r\n  max-width: 350px;\r\n  margin: 0 auto;\r\n  margin-top: 35px;\r\n  cursor: pointer;\r\n  transition: box-shadow 200ms ease-out;\r\n}\r\n\r\n.appartment-box[_ngcontent-%COMP%]:hover {\r\n  box-shadow: 1px 4px 10px rgba(136, 136, 136, 0.836);\r\n  transition: box-shadow 200ms ease-in;\r\n}\r\n\r\n.appartment-image[_ngcontent-%COMP%] {\r\n  height: 76%;\r\n}\r\n\r\n.appartment-image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.appartment-info[_ngcontent-%COMP%] {\r\n  height: 24%;\r\n  background-color: #fff;\r\n  padding: 25px 20px;\r\n}\r\n\r\n.appartment-title[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 50%;\r\n}\r\n\r\n.appartment-title[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 15px;\r\n  font-weight: 700;\r\n  letter-spacing: 0px;\r\n  line-height: 20px;\r\n}\r\n\r\n.appartment-details[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 50%;\r\n  color: #e0a400;\r\n\r\n}\r\n\r\n.price[_ngcontent-%COMP%] {\r\n  width: 50%;\r\n  height: 100%;\r\n}\r\n\r\n.price[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 18px;\r\n  color: #ffd000;\r\n  font-weight: 700;\r\n}\r\n\r\n.bathrooms[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  height: 100%;\r\n}\r\n\r\n.bathrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 0px 10px;\r\n  font-size: 13px;\r\n  color: #919EB1;\r\n}\r\n\r\n.bedrooms[_ngcontent-%COMP%] {\r\n  width: 25%;\r\n  height: 100%\r\n}\r\n\r\n.bedrooms[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  padding: 15px 0px 0px 10px;\r\n  font-size: 13px;\r\n  color: #919EB1;\r\n}\r\n\r\n.search-appartments[_ngcontent-%COMP%] {\r\n  position: inherit;\r\n}\r\n\r\n.search-all-btn[_ngcontent-%COMP%] {\r\n  margin-left: 20px;\r\n  border-radius: 5px;\r\n  background-color: #000;\r\n}\r\n\r\n\r\n\r\nfooter[_ngcontent-%COMP%] {\r\n  background-color: #333333;\r\n  width: 100%;\r\n  color: #fff;\r\n  padding: 65px 0px;\r\n}\r\n\r\n.footer-top[_ngcontent-%COMP%] {\r\n  border-bottom: 1px solid #919EB1;\r\n  padding-bottom: 45px;\r\n}\r\n\r\n.Newsletter[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n  width: 80%;\r\n  height: 70px;\r\n  padding: 0px 15px;\r\n  border: 0px;\r\n  margin-top: 26px;\r\n}\r\n\r\n.newsletter-btn[_ngcontent-%COMP%] {\r\n  background-color: #fbfbfb;\r\n  width: 20%;\r\n  height: 70px;\r\n  border: 0px;\r\n  background-image: url('arrow-right.svg');\r\n  background-repeat: no-repeat;\r\n  background-position: center;\r\n  margin-top: 26px;\r\n}\r\n\r\n.newsletter-btn[_ngcontent-%COMP%]:hover {\r\n  background-color: #ffd000;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n\r\n.footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n  margin-bottom: 60px;\r\n}\r\n\r\n.footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n  font-size: 13px;\r\n  font-weight: 400;\r\n}\r\n\r\n.footer-bottom[_ngcontent-%COMP%] {\r\n  padding-top: 70px;\r\n}\r\n\r\n.footer-bottom[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: white;\r\n  font-size: 15px;\r\n}\r\n\r\n.footer-column[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%] {\r\n  padding-bottom: 35px;\r\n}\r\n\r\n.footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n  color: #fff;\r\n  display: flex;\r\n  font-size: 13px;\r\n  padding-bottom: 18px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n@media only screen and (max-width: 992px) {\r\n  .menu[_ngcontent-%COMP%] {\r\n    background-color: #fff;\r\n    width: 100%;\r\n    height: 100vh;\r\n    position: absolute;\r\n    top: 70px;\r\n    right: 100%;\r\n    text-align: center;\r\n    z-index: 999;\r\n    transition: all 0.4s ease-in-out;\r\n  }\r\n\r\n  .page-menu[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    height: 50vh;\r\n    text-align: center;\r\n    float: none;\r\n    display: flex;\r\n    flex-direction: column;\r\n  }\r\n\r\n  .page-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    font-size: 15pt;\r\n    padding: 1.5vh;\r\n  }\r\n\r\n  .registration[_ngcontent-%COMP%] {\r\n    float: none;\r\n    width: 100%;\r\n    height: 50vh;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    text-align: center;\r\n    padding-bottom: 5vh;\r\n  }\r\n\r\n  .join-us[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    text-align: center;\r\n    height: 0px;\r\n    margin-top: 30px;\r\n  }\r\n\r\n  .join-us[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    text-align: center;\r\n  }\r\n\r\n  .menu-button[_ngcontent-%COMP%] {\r\n    width: 40px;\r\n    height: 40px;\r\n    display: block;\r\n    margin-top: 26px;\r\n  }\r\n\r\n  .menu-button[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\r\n    width: 40px;\r\n    height: 5px;\r\n    background-color: #1F373D;\r\n    display: block;\r\n    margin: 6px 0px;\r\n  }\r\n\r\n  .main-btn[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    background-color: #ffd000;\r\n    padding: 10px 10px;\r\n    margin: 0px;\r\n    border-radius: 5px;\r\n    font-size: 13px;\r\n    border: 1px solid #ffd000;\r\n    transition: 0.4s all;\r\n  }\r\n\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(1) {\r\n    float: left;\r\n    margin-top: 50px;\r\n  }\r\n\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(2) {\r\n    margin-top: 50px;\r\n  }\r\n\r\n  .statistic-box[_ngcontent-%COMP%]:nth-child(3) {\r\n    float: left;\r\n  }\r\n\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 300px;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 30%;\r\n  }\r\n\r\n  .how-it-works[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 768px) {\r\n  .hero-image[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n  }\r\n\r\n  .info[_ngcontent-%COMP%] {\r\n    margin: 0px 10px;\r\n  }\r\n\r\n  h1[_ngcontent-%COMP%] {\r\n    font-size: 45px;\r\n  }\r\n\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 270px;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 37%;\r\n    margin-left: 10px;\r\n  }\r\n\r\n  .work-service-image[_ngcontent-%COMP%] {\r\n    max-height: 630px;\r\n    left: 0px;\r\n    margin-bottom: 50px;\r\n  }\r\n\r\n  .footer-logo[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n    float: left;\r\n  }\r\n\r\n  .footer-logo[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    float: right;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 650px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 14px;\r\n  }\r\n\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    right: 40px;\r\n  }\r\n\r\n  .title[_ngcontent-%COMP%] {\r\n    top: 17vh;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 40%;\r\n    margin-left: 5px;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 575px) {\r\n  .footer-bottom[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n  }\r\n\r\n  .footer-column[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    display: list-item;\r\n    list-style: none;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 540px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 12px;\r\n  }\r\n\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    width: 90%;\r\n  }\r\n\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 400px;\r\n  }\r\n\r\n  .search-appartments[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n    margin: 0 auto;\r\n    margin-top: 30px;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n    margin: 0px 10%;\r\n    margin-top: 15px;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 470px) {\r\n  .info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 12px;\r\n  }\r\n\r\n  .hero-image-info[_ngcontent-%COMP%] {\r\n    right: 30px;\r\n    width: 91%;\r\n  }\r\n\r\n  .hero-image-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    padding: 5px 5px;\r\n    font-size: 11px;\r\n  }\r\n\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 300px;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n  }\r\n}\r\n\r\n@media only screen and (max-width: 380px) {\r\n  .title[_ngcontent-%COMP%] {\r\n    top: 22vh;\r\n  }\r\n\r\n  .search[_ngcontent-%COMP%] {\r\n    width: 250px;\r\n  }\r\n\r\n  .search-btn[_ngcontent-%COMP%] {\r\n    width: 30%;\r\n  }\r\n\r\n  .search-all-btn[_ngcontent-%COMP%] {\r\n    width: 80%;\r\n  }\r\n\r\n  .search[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n    width: 70%;\r\n    font-size: 12px;\r\n    padding: 27px 40px;\r\n  }\r\n\r\n  .info[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n    display: none;\r\n  }\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFsbC1wcm9wZXJ0aWVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQkFBb0I7RUFDcEIsVUFBVTtBQUNaOztBQUVBO0VBR0Usc0JBQXNCO0FBQ3hCOztBQUVBOzs7RUFLRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxtQ0FBbUM7RUFDbkMsa0NBQWtDO0VBQ2xDLGtDQUFrQztFQUNsQyx1Q0FBdUM7QUFDekM7O0FBRUE7O0VBRUUscUJBQXFCO0VBQ3JCLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsWUFBWTtBQUNkOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsV0FBVztFQUNYLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsaUJBQWlCO0FBQ25COztBQUVBOztFQUVFLHdCQUF3QjtBQUMxQjs7QUFFQTtFQUNFLDJCQUEyQjtBQUM3Qjs7QUFFQTtFQUNFLDRCQUE0QjtBQUM5Qjs7QUFFQTtFQUNFLDZCQUE2QjtBQUMvQjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsT0FBTztFQUdQLGdDQUFnQztBQUNsQzs7QUFFQTs7NkRBRTZEOztBQUM3RDtFQUNFLGNBQWM7RUFDZCxxQ0FBcUM7RUFDckMsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsc0JBQXNCO0FBQ3hCOztBQUVBOztFQUVFLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7O0FBRUE7O0VBRUUsZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjs7QUFFQTs7RUFFRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCOztBQUVBOztFQUVFLGVBQWU7RUFDZixnQkFBZ0I7QUFDbEI7O0FBRUE7O0VBRUUsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCOztBQUVBOztFQUVFLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQix1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHVCQUF1QjtFQUN2QixlQUFlO0FBQ2pCOztBQUVBO0VBR0UsZ0NBQWdDO0FBQ2xDOztBQUVBO0VBR0UsbURBQW1EO0FBQ3JEOztBQUVBO0VBQ0Usb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2YsYUFBYTtFQUNiLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUdFLGdDQUFnQztBQUNsQzs7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7QUFDekI7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtBQUNkOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLFVBQVU7RUFDVixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxVQUFVO0VBQ1YsWUFBWTtFQUNaLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLFVBQVU7RUFDVixZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsNkJBQTZCO0VBQzdCLGNBQWM7RUFDZCx5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsY0FBYztBQUNoQjs7QUFHQTtFQUNFLHlCQUF5QjtFQUN6QiwwQkFBMEI7QUFDNUI7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFHRSxrREFBa0Q7RUFDbEQsV0FBVztFQUNYLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixlQUFlO0VBRWYscUNBQXFDO0FBQ3ZDOztBQUVBO0VBQ0UsbURBQW1EO0VBRW5ELG9DQUFvQztBQUN0Qzs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLGNBQWM7O0FBRWhCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGVBQWU7RUFDZixjQUFjO0VBQ2QsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtFQUNFLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsY0FBYztBQUNoQjs7QUFFQTtFQUNFLFVBQVU7RUFDVjtBQUNGOztBQUVBO0VBQ0UsMEJBQTBCO0VBQzFCLGVBQWU7RUFDZixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixzQkFBc0I7QUFDeEI7O0FBRUE7OzhEQUU4RDs7QUFDOUQ7RUFDRSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxnQ0FBZ0M7RUFDaEMsb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsV0FBVztFQUNYLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixVQUFVO0VBQ1YsWUFBWTtFQUNaLFdBQVc7RUFDWCx3Q0FBNEQ7RUFDNUQsNEJBQTRCO0VBQzVCLDJCQUEyQjtFQUMzQixnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSx5QkFBeUI7RUFHekIsZ0NBQWdDO0FBQ2xDOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLFlBQVk7RUFDWixlQUFlO0FBQ2pCOztBQUVBO0VBQ0Usb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixlQUFlO0VBQ2Ysb0JBQW9CO0FBQ3RCOztBQUVBOztnRUFFZ0U7O0FBQ2hFLG1EQUFtRDs7QUFDbkQ7RUFDRTtJQUNFLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixZQUFZO0lBR1osZ0NBQWdDO0VBQ2xDOztFQUVBO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGFBQWE7SUFDYixzQkFBc0I7RUFDeEI7O0VBRUE7SUFDRSxlQUFlO0lBQ2YsY0FBYztFQUNoQjs7RUFFQTtJQUNFLFdBQVc7SUFDWCxXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixtQkFBbUI7RUFDckI7O0VBRUE7SUFDRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxnQkFBZ0I7RUFDbEI7O0VBRUE7SUFDRSxXQUFXO0lBQ1gsa0JBQWtCO0VBQ3BCOztFQUVBO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWixjQUFjO0lBQ2QsZ0JBQWdCO0VBQ2xCOztFQUVBO0lBQ0UsV0FBVztJQUNYLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIsY0FBYztJQUNkLGVBQWU7RUFDakI7O0VBRUE7SUFDRSxXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZix5QkFBeUI7SUFDekIsb0JBQW9CO0VBQ3RCOztFQUVBO0lBQ0UsV0FBVztJQUNYLGdCQUFnQjtFQUNsQjs7RUFFQTtJQUNFLGdCQUFnQjtFQUNsQjs7RUFFQTtJQUNFLFdBQVc7RUFDYjs7RUFFQTtJQUNFLFlBQVk7RUFDZDs7RUFFQTtJQUNFLFVBQVU7RUFDWjs7RUFFQTtJQUNFLGtCQUFrQjtFQUNwQjtBQUNGOztBQUVBO0VBQ0U7SUFDRSxXQUFXO0VBQ2I7O0VBRUE7SUFDRSxnQkFBZ0I7RUFDbEI7O0VBRUE7SUFDRSxlQUFlO0VBQ2pCOztFQUVBO0lBQ0UsWUFBWTtFQUNkOztFQUVBO0lBQ0UsVUFBVTtJQUNWLGlCQUFpQjtFQUNuQjs7RUFFQTtJQUNFLGlCQUFpQjtJQUNqQixTQUFTO0lBQ1QsbUJBQW1CO0VBQ3JCOztFQUVBO0lBQ0UsV0FBVztFQUNiOztFQUVBO0lBQ0UsWUFBWTtFQUNkO0FBQ0Y7O0FBRUE7RUFDRTtJQUNFLGVBQWU7RUFDakI7O0VBRUE7SUFDRSxXQUFXO0VBQ2I7O0VBRUE7SUFDRSxTQUFTO0VBQ1g7O0VBRUE7SUFDRSxVQUFVO0lBQ1YsZ0JBQWdCO0VBQ2xCO0FBQ0Y7O0FBRUE7RUFDRTtJQUNFLGtCQUFrQjtFQUNwQjs7RUFFQTtJQUNFLGtCQUFrQjtJQUNsQixnQkFBZ0I7RUFDbEI7QUFDRjs7QUFFQTtFQUNFO0lBQ0UsZUFBZTtFQUNqQjs7RUFFQTtJQUNFLFVBQVU7RUFDWjs7RUFFQTtJQUNFLFlBQVk7RUFDZDs7RUFFQTtJQUNFLFVBQVU7SUFDVixjQUFjO0lBQ2QsZ0JBQWdCO0VBQ2xCOztFQUVBO0lBQ0UsVUFBVTtJQUNWLGVBQWU7SUFDZixnQkFBZ0I7RUFDbEI7QUFDRjs7QUFFQTtFQUNFO0lBQ0UsZUFBZTtFQUNqQjs7RUFFQTtJQUNFLFdBQVc7SUFDWCxVQUFVO0VBQ1o7O0VBRUE7SUFDRSxnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjs7RUFFQTtJQUNFLFlBQVk7RUFDZDs7RUFFQTtJQUNFLFVBQVU7RUFDWjtBQUNGOztBQUVBO0VBQ0U7SUFDRSxTQUFTO0VBQ1g7O0VBRUE7SUFDRSxZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxVQUFVO0VBQ1o7O0VBRUE7SUFDRSxVQUFVO0VBQ1o7O0VBRUE7SUFDRSxVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQjtFQUNwQjs7RUFFQTtJQUNFLGFBQWE7RUFDZjtBQUNGIiwiZmlsZSI6ImFsbC1wcm9wZXJ0aWVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqIHtcclxuICAvKiBtYXJnaW46IDAgYXV0bzsgKi9cclxuICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG5odG1sIHtcclxuICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbiosXHJcbio6YmVmb3JlLFxyXG4qOmFmdGVyIHtcclxuICAtd2Via2l0LWJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgLW1vei1ib3gtc2l6aW5nOiBpbmhlcml0O1xyXG4gIGJveC1zaXppbmc6IGluaGVyaXQ7XHJcbn1cclxuXHJcbmEge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgY29sb3I6ICMxRjM3M0Q7XHJcbn1cclxuXHJcbmJvZHkge1xyXG4gIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xyXG4gIC1tb3otb3N4LWZvbnQtc21vb3RoaW5nOiBncmF5c2NhbGU7XHJcbiAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcclxuICB0ZXh0LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAuMDEpIDAgMCAxcHg7XHJcbn1cclxuXHJcbmE6Zm9jdXMsXHJcbmE6aG92ZXIge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBjb2xvcjogI2ZmZDAwMDtcclxufVxyXG5cclxuaW1nIHtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG59XHJcblxyXG46OnNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOjotbW96LXNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOjotd2Via2l0LXNlbGVjdGlvbiB7XHJcbiAgYmFja2dyb3VuZDogIzFGMzczRDtcclxuICBjb2xvcjogI2ZmZjtcclxuICB0ZXh0LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuOmFjdGl2ZSxcclxuOmZvY3VzIHtcclxuICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWxlZnQge1xyXG4gIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtcmlnaHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWNlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5sZWZ0IHtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLnJpZ2h0IHtcclxuICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5jZW50ZXIge1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG59XHJcblxyXG4uZmxleC1jZW50ZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLnVwcGVyY2FzZSB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuLndoaXRlLXRleHQge1xyXG4gIGNvbG9yOiAjRkZGO1xyXG59XHJcblxyXG4ubGlnaHQtYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi55ZWxsb3ctYmcge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmQwMDA7XHJcbn1cclxuXHJcbi5wb2ludGVyIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5oaWRlIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uc2hvdyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5zbGlkZSB7XHJcbiAgbGVmdDogMDtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNHMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuXHJcbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIDIuIFR5cG9ncmFwaHkgXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmJvZHkge1xyXG4gIGNvbG9yOiAjMUYzNzNEO1xyXG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOiAxM3B0O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMXM7XHJcbn1cclxuXHJcbmgxLFxyXG4uaDEge1xyXG4gIGZvbnQtc2l6ZTogNThweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5oMixcclxuLmgyIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuaDMsXHJcbi5oMyB7XHJcbiAgZm9udC1zaXplOiA0MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbmg0LFxyXG4uaDQge1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG5oNSxcclxuLmg1IHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBjb2xvcjogIzkxOUVCMTtcclxufVxyXG5cclxuaDYsXHJcbi5oNiB7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG59XHJcblxyXG5wIHtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDEuODtcclxuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjAyNWVtO1xyXG59XHJcblxyXG5saSB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDI1ZW07XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG59XHJcblxyXG4uYW5pbWF0ZSB7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4ubHV4LXNoYWRvdyB7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCAxNXB4IDBweCByZ2JhKDI5LCAzMSwgMzYsIDAuMSk7XHJcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMTBweCAxNXB4IDBweCByZ2JhKDI5LCAzMSwgMzYsIDAuMSk7XHJcbiAgYm94LXNoYWRvdzogMHB4IDEwcHggMTVweCAwcHggcmdiYSgyOSwgMzEsIDM2LCAwLjEpO1xyXG59XHJcblxyXG5oZWFkZXIge1xyXG4gIHBhZGRpbmctYm90dG9tOiAyNXB4O1xyXG59XHJcblxyXG5oZWFkZXIgbmF2IHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHotaW5kZXg6IDk5OTk7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLmFuaW1hdGUge1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLnNtYWxsLW5hdiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmxvZ28ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgZm9udC1zaXplOiAxOHB0O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIC8qIGhlaWdodDogOTBweDsgKi9cclxufVxyXG5cclxuLm1lbnUge1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG59XHJcblxyXG4ucGFnZS1tZW51IHtcclxuICB3aWR0aDogNzAlO1xyXG4gIGhlaWdodDogOTVweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5yZWdpc3RyYXRpb24ge1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgaGVpZ2h0OiA5NXB4O1xyXG59XHJcblxyXG4uam9pbi11cyB7XHJcbiAgd2lkdGg6IDQwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgcGFkZGluZy1yaWdodDogMjBweDtcclxufVxyXG5cclxuLmdldHRpbmctc3RhcnRlZCB7XHJcbiAgd2lkdGg6IDYwJTtcclxuICBoZWlnaHQ6IDk1cHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbn1cclxuXHJcbi5tYWluLWJ0biB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZDAwMDtcclxuICBwYWRkaW5nOiAxMHB4IDEwcHg7XHJcbiAgbWFyZ2luOiAwcHggNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZDAwMDtcclxuICB0cmFuc2l0aW9uOiAwLjRzIGFsbDtcclxufVxyXG5cclxuLm1haW4tYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICBjb2xvcjogIzAwMDAwMDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZmZkMDAwO1xyXG59XHJcblxyXG4ubWFpbi1idG46aG92ZXIgYSB7XHJcbiAgY29sb3I6ICMxRjM3M0Q7XHJcbn1cclxuXHJcbi5tYWluLWJ0biBhIHtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuLnBhZ2UtbWVudSBsaSB7XHJcbiAgcGFkZGluZzogOHB4O1xyXG59XHJcblxyXG4uYWN0aXZlIHtcclxuICBjb2xvcjogI2ZmZDAwMDtcclxufVxyXG5cclxuXHJcbi5hcHBhcnRtZW50cyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y2RjVGNDtcclxuICBwYWRkaW5nOiA2MHB4IDBweCA3MHB4IDBweDtcclxufVxyXG5cclxuLmFwcGFydG1lbnRzIGgyIHtcclxuICBtYXJnaW4tYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4uYXBwYXJ0bWVudC1ib3gge1xyXG4gIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCA4cHggMHB4IHJnYmEoMjQsIDQ5LCA1NiwgMC4xNSk7XHJcbiAgLW1vei1ib3gtc2hhZG93OiAwcHggMnB4IDhweCAwcHggcmdiYSgyNCwgNDksIDU2LCAwLjE1KTtcclxuICBib3gtc2hhZG93OiAwcHggMnB4IDhweCAwcHggcmdiYSgyNCwgNDksIDU2LCAwLjE1KTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDUwMHB4O1xyXG4gIG1heC13aWR0aDogMzUwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMzVweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBib3gtc2hhZG93IDIwMG1zIGVhc2Utb3V0O1xyXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMjAwbXMgZWFzZS1vdXQ7XHJcbn1cclxuXHJcbi5hcHBhcnRtZW50LWJveDpob3ZlciB7XHJcbiAgYm94LXNoYWRvdzogMXB4IDRweCAxMHB4IHJnYmEoMTM2LCAxMzYsIDEzNiwgMC44MzYpO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYm94LXNoYWRvdyAyMDBtcyBlYXNlLWluO1xyXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMjAwbXMgZWFzZS1pbjtcclxufVxyXG5cclxuLmFwcGFydG1lbnQtaW1hZ2Uge1xyXG4gIGhlaWdodDogNzYlO1xyXG59XHJcblxyXG4uYXBwYXJ0bWVudC1pbWFnZSBpbWcge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLmFwcGFydG1lbnQtaW5mbyB7XHJcbiAgaGVpZ2h0OiAyNCU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICBwYWRkaW5nOiAyNXB4IDIwcHg7XHJcbn1cclxuXHJcbi5hcHBhcnRtZW50LXRpdGxlIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDUwJTtcclxufVxyXG5cclxuLmFwcGFydG1lbnQtdGl0bGUgcCB7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcclxuICBsaW5lLWhlaWdodDogMjBweDtcclxufVxyXG5cclxuLmFwcGFydG1lbnQtZGV0YWlscyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA1MCU7XHJcbiAgY29sb3I6ICNlMGE0MDA7XHJcblxyXG59XHJcblxyXG4ucHJpY2Uge1xyXG4gIHdpZHRoOiA1MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4ucHJpY2UgcCB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGNvbG9yOiAjZmZkMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5iYXRocm9vbXMge1xyXG4gIHdpZHRoOiAyNSU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uYmF0aHJvb21zIHAge1xyXG4gIHBhZGRpbmc6IDE1cHggMHB4IDBweCAxMHB4O1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBjb2xvcjogIzkxOUVCMTtcclxufVxyXG5cclxuLmJlZHJvb21zIHtcclxuICB3aWR0aDogMjUlO1xyXG4gIGhlaWdodDogMTAwJVxyXG59XHJcblxyXG4uYmVkcm9vbXMgcCB7XHJcbiAgcGFkZGluZzogMTVweCAwcHggMHB4IDEwcHg7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGNvbG9yOiAjOTE5RUIxO1xyXG59XHJcblxyXG4uc2VhcmNoLWFwcGFydG1lbnRzIHtcclxuICBwb3NpdGlvbjogaW5oZXJpdDtcclxufVxyXG5cclxuLnNlYXJjaC1hbGwtYnRuIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxufVxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgOS4gRm9vdGVyXHJcbi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG5mb290ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzMzMzMzM7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogNjVweCAwcHg7XHJcbn1cclxuXHJcbi5mb290ZXItdG9wIHtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzkxOUVCMTtcclxuICBwYWRkaW5nLWJvdHRvbTogNDVweDtcclxufVxyXG5cclxuLk5ld3NsZXR0ZXIgaW5wdXQge1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgaGVpZ2h0OiA3MHB4O1xyXG4gIHBhZGRpbmc6IDBweCAxNXB4O1xyXG4gIGJvcmRlcjogMHB4O1xyXG4gIG1hcmdpbi10b3A6IDI2cHg7XHJcbn1cclxuXHJcbi5uZXdzbGV0dGVyLWJ0biB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZiZmJmYjtcclxuICB3aWR0aDogMjAlO1xyXG4gIGhlaWdodDogNzBweDtcclxuICBib3JkZXI6IDBweDtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvYXJyb3ctcmlnaHQuc3ZnJyk7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgbWFyZ2luLXRvcDogMjZweDtcclxufVxyXG5cclxuLm5ld3NsZXR0ZXItYnRuOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmZvb3Rlci1sb2dvIGg0IHtcclxuICBtYXJnaW4tYm90dG9tOiA2MHB4O1xyXG59XHJcblxyXG4uZm9vdGVyLWxvZ28gcCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuXHJcbi5mb290ZXItYm90dG9tIHtcclxuICBwYWRkaW5nLXRvcDogNzBweDtcclxufVxyXG5cclxuLmZvb3Rlci1ib3R0b20gYSB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLmZvb3Rlci1jb2x1bW4gaDUge1xyXG4gIHBhZGRpbmctYm90dG9tOiAzNXB4O1xyXG59XHJcblxyXG4uZm9vdGVyLWNvbHVtbiBhIHtcclxuICBjb2xvcjogI2ZmZjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMThweDtcclxufVxyXG5cclxuLyotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICAxMC4gUkVTUE9OU0lWRVxyXG4gIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4vKiBMYXJnZSBkZXZpY2VzIChsYXB0b3BzL2Rlc2t0b3BzLCA5OTJweCBhbmQgdXApICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAubWVudSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA3MHB4O1xyXG4gICAgcmlnaHQ6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBhbGwgMC40cyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjRzIGVhc2UtaW4tb3V0O1xyXG4gIH1cclxuXHJcbiAgLnBhZ2UtbWVudSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNTB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZsb2F0OiBub25lO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgfVxyXG5cclxuICAucGFnZS1tZW51IGxpIHtcclxuICAgIGZvbnQtc2l6ZTogMTVwdDtcclxuICAgIHBhZGRpbmc6IDEuNXZoO1xyXG4gIH1cclxuXHJcbiAgLnJlZ2lzdHJhdGlvbiB7XHJcbiAgICBmbG9hdDogbm9uZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MHZoO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNXZoO1xyXG4gIH1cclxuXHJcbiAgLmpvaW4tdXMge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgfVxyXG5cclxuICAuam9pbi11cyBsaSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG4gIC5tZW51LWJ1dHRvbiB7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luLXRvcDogMjZweDtcclxuICB9XHJcblxyXG4gIC5tZW51LWJ1dHRvbiBzcGFuIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUYzNzNEO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW46IDZweCAwcHg7XHJcbiAgfVxyXG5cclxuICAubWFpbi1idG4ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkMDAwO1xyXG4gICAgcGFkZGluZzogMTBweCAxMHB4O1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZkMDAwO1xyXG4gICAgdHJhbnNpdGlvbjogMC40cyBhbGw7XHJcbiAgfVxyXG5cclxuICAuc3RhdGlzdGljLWJveDpudGgtY2hpbGQoMSkge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gIH1cclxuXHJcbiAgLnN0YXRpc3RpYy1ib3g6bnRoLWNoaWxkKDIpIHtcclxuICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgfVxyXG5cclxuICAuc3RhdGlzdGljLWJveDpudGgtY2hpbGQoMykge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoLWFwcGFydG1lbnRzIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gIH1cclxuXHJcbiAgLmhvdy1pdC13b3JrcyB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgLmhlcm8taW1hZ2Uge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICAuaW5mbyB7XHJcbiAgICBtYXJnaW46IDBweCAxMHB4O1xyXG4gIH1cclxuXHJcbiAgaDEge1xyXG4gICAgZm9udC1zaXplOiA0NXB4O1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaC1hcHBhcnRtZW50cyB7XHJcbiAgICB3aWR0aDogMjcwcHg7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gIH1cclxuXHJcbiAgLndvcmstc2VydmljZS1pbWFnZSB7XHJcbiAgICBtYXgtaGVpZ2h0OiA2MzBweDtcclxuICAgIGxlZnQ6IDBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG5cclxuICAuZm9vdGVyLWxvZ28gaDQge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAuZm9vdGVyLWxvZ28gcCB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDY1MHB4KSB7XHJcbiAgLmluZm8gcCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgfVxyXG5cclxuICAuaGVyby1pbWFnZS1pbmZvIHtcclxuICAgIHJpZ2h0OiA0MHB4O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlIHtcclxuICAgIHRvcDogMTd2aDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICB3aWR0aDogNDAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTc1cHgpIHtcclxuICAuZm9vdGVyLWJvdHRvbSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICAuZm9vdGVyLWNvbHVtbiBhIHtcclxuICAgIGRpc3BsYXk6IGxpc3QtaXRlbTtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU0MHB4KSB7XHJcbiAgLmluZm8gcCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgfVxyXG5cclxuICAuaGVyby1pbWFnZS1pbmZvIHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiA0MDBweDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYXBwYXJ0bWVudHMge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgbWFyZ2luOiAwcHggMTAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDcwcHgpIHtcclxuICAuaW5mbyBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICB9XHJcblxyXG4gIC5oZXJvLWltYWdlLWluZm8ge1xyXG4gICAgcmlnaHQ6IDMwcHg7XHJcbiAgICB3aWR0aDogOTElO1xyXG4gIH1cclxuXHJcbiAgLmhlcm8taW1hZ2UtaW5mbyBwIHtcclxuICAgIHBhZGRpbmc6IDVweCA1cHg7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYWxsLWJ0biB7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzODBweCkge1xyXG4gIC50aXRsZSB7XHJcbiAgICB0b3A6IDIydmg7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoIHtcclxuICAgIHdpZHRoOiAyNTBweDtcclxuICB9XHJcblxyXG4gIC5zZWFyY2gtYnRuIHtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoLWFsbC1idG4ge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICB9XHJcblxyXG4gIC5zZWFyY2ggaW5wdXQge1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHBhZGRpbmc6IDI3cHggNDBweDtcclxuICB9XHJcblxyXG4gIC5pbmZvIGltZyB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxufVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AllPropertiesComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-all-properties',
                templateUrl: './all-properties.component.html',
                styleUrls: ['./all-properties.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _all_properties_all_properties_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./all-properties/all-properties.component */ "jMce");
/* harmony import */ var _property_property_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./property/property.component */ "6Y1P");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "9vUh");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about/about.component */ "84zG");








const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'all', component: _all_properties_all_properties_component__WEBPACK_IMPORTED_MODULE_2__["AllPropertiesComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_5__["AboutComponent"] },
    { path: 'property/:id', component: _property_property_component__WEBPACK_IMPORTED_MODULE_3__["PropertyComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map